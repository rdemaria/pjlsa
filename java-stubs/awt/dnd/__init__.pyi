from typing import Any as _py_Any
from typing import List as _py_List
from typing import TypeVar as _py_TypeVar
from typing import Type as _py_Type
from typing import ClassVar as _py_ClassVar
from typing import overload
import java.awt
import java.awt.datatransfer
import java.awt.dnd.peer
import java.awt.event
import java.awt.peer
import java.io
import java.lang
import java.util


class Autoscroll:
    def autoscroll(self, point: java.awt.Point) -> None: ...
    def getAutoscrollInsets(self) -> java.awt.Insets: ...

class DnDConstants:
    ACTION_NONE: _py_ClassVar[int] = ...
    ACTION_COPY: _py_ClassVar[int] = ...
    ACTION_MOVE: _py_ClassVar[int] = ...
    ACTION_COPY_OR_MOVE: _py_ClassVar[int] = ...
    ACTION_LINK: _py_ClassVar[int] = ...
    ACTION_REFERENCE: _py_ClassVar[int] = ...

class DragGestureEvent(java.util.EventObject):
    def __init__(self, dragGestureRecognizer: 'DragGestureRecognizer', int: int, point: java.awt.Point, list: java.util.List[java.awt.event.InputEvent]): ...
    def getComponent(self) -> java.awt.Component: ...
    def getDragAction(self) -> int: ...
    def getDragOrigin(self) -> java.awt.Point: ...
    def getDragSource(self) -> 'DragSource': ...
    def getSourceAsDragGestureRecognizer(self) -> 'DragGestureRecognizer': ...
    def getTriggerEvent(self) -> java.awt.event.InputEvent: ...
    def iterator(self) -> java.util.Iterator[java.awt.event.InputEvent]: ...
    @overload
    def startDrag(self, cursor: java.awt.Cursor, image: java.awt.Image, point: java.awt.Point, transferable: java.awt.datatransfer.Transferable, dragSourceListener: 'DragSourceListener') -> None: ...
    @overload
    def startDrag(self, cursor: java.awt.Cursor, transferable: java.awt.datatransfer.Transferable) -> None: ...
    @overload
    def startDrag(self, cursor: java.awt.Cursor, transferable: java.awt.datatransfer.Transferable, dragSourceListener: 'DragSourceListener') -> None: ...
    @overload
    def toArray(self) -> _py_List[_py_Any]: ...
    @overload
    def toArray(self, objectArray: _py_List[_py_Any]) -> _py_List[_py_Any]: ...

class DragGestureListener(java.util.EventListener):
    def dragGestureRecognized(self, dragGestureEvent: DragGestureEvent) -> None: ...

class DragGestureRecognizer(java.io.Serializable):
    def addDragGestureListener(self, dragGestureListener: DragGestureListener) -> None: ...
    def getComponent(self) -> java.awt.Component: ...
    def getDragSource(self) -> 'DragSource': ...
    def getSourceActions(self) -> int: ...
    def getTriggerEvent(self) -> java.awt.event.InputEvent: ...
    def removeDragGestureListener(self, dragGestureListener: DragGestureListener) -> None: ...
    def resetRecognizer(self) -> None: ...
    def setComponent(self, component: java.awt.Component) -> None: ...
    def setSourceActions(self, int: int) -> None: ...

class DragSource(java.io.Serializable):
    DefaultCopyDrop: _py_ClassVar[java.awt.Cursor] = ...
    DefaultMoveDrop: _py_ClassVar[java.awt.Cursor] = ...
    DefaultLinkDrop: _py_ClassVar[java.awt.Cursor] = ...
    DefaultCopyNoDrop: _py_ClassVar[java.awt.Cursor] = ...
    DefaultMoveNoDrop: _py_ClassVar[java.awt.Cursor] = ...
    DefaultLinkNoDrop: _py_ClassVar[java.awt.Cursor] = ...
    def __init__(self): ...
    def addDragSourceListener(self, dragSourceListener: 'DragSourceListener') -> None: ...
    def addDragSourceMotionListener(self, dragSourceMotionListener: 'DragSourceMotionListener') -> None: ...
    def createDefaultDragGestureRecognizer(self, component: java.awt.Component, int: int, dragGestureListener: DragGestureListener) -> DragGestureRecognizer: ...
    _createDragGestureRecognizer__T = _py_TypeVar('_createDragGestureRecognizer__T', bound=DragGestureRecognizer)  # <T>
    def createDragGestureRecognizer(self, class_: _py_Type[_createDragGestureRecognizer__T], component: java.awt.Component, int: int, dragGestureListener: DragGestureListener) -> _createDragGestureRecognizer__T: ...
    @classmethod
    def getDefaultDragSource(cls) -> 'DragSource': ...
    def getDragSourceListeners(self) -> _py_List['DragSourceListener']: ...
    def getDragSourceMotionListeners(self) -> _py_List['DragSourceMotionListener']: ...
    @classmethod
    def getDragThreshold(cls) -> int: ...
    def getFlavorMap(self) -> java.awt.datatransfer.FlavorMap: ...
    _getListeners__T = _py_TypeVar('_getListeners__T', bound=java.util.EventListener)  # <T>
    def getListeners(self, class_: _py_Type[_getListeners__T]) -> _py_List[_getListeners__T]: ...
    @classmethod
    def isDragImageSupported(cls) -> bool: ...
    def removeDragSourceListener(self, dragSourceListener: 'DragSourceListener') -> None: ...
    def removeDragSourceMotionListener(self, dragSourceMotionListener: 'DragSourceMotionListener') -> None: ...
    @overload
    def startDrag(self, dragGestureEvent: DragGestureEvent, cursor: java.awt.Cursor, image: java.awt.Image, point: java.awt.Point, transferable: java.awt.datatransfer.Transferable, dragSourceListener: 'DragSourceListener') -> None: ...
    @overload
    def startDrag(self, dragGestureEvent: DragGestureEvent, cursor: java.awt.Cursor, image: java.awt.Image, point: java.awt.Point, transferable: java.awt.datatransfer.Transferable, dragSourceListener: 'DragSourceListener', flavorMap: java.awt.datatransfer.FlavorMap) -> None: ...
    @overload
    def startDrag(self, dragGestureEvent: DragGestureEvent, cursor: java.awt.Cursor, transferable: java.awt.datatransfer.Transferable, dragSourceListener: 'DragSourceListener') -> None: ...
    @overload
    def startDrag(self, dragGestureEvent: DragGestureEvent, cursor: java.awt.Cursor, transferable: java.awt.datatransfer.Transferable, dragSourceListener: 'DragSourceListener', flavorMap: java.awt.datatransfer.FlavorMap) -> None: ...

class DragSourceEvent(java.util.EventObject):
    @overload
    def __init__(self, dragSourceContext: 'DragSourceContext'): ...
    @overload
    def __init__(self, dragSourceContext: 'DragSourceContext', int: int, int2: int): ...
    def getDragSourceContext(self) -> 'DragSourceContext': ...
    def getLocation(self) -> java.awt.Point: ...
    def getX(self) -> int: ...
    def getY(self) -> int: ...

class DragSourceListener(java.util.EventListener):
    def dragDropEnd(self, dragSourceDropEvent: 'DragSourceDropEvent') -> None: ...
    def dragEnter(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragExit(self, dragSourceEvent: DragSourceEvent) -> None: ...
    def dragOver(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dropActionChanged(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...

class DragSourceMotionListener(java.util.EventListener):
    def dragMouseMoved(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...

class DropTargetContext(java.io.Serializable):
    def addNotify(self, dropTargetContextPeer: java.awt.dnd.peer.DropTargetContextPeer) -> None: ...
    def dropComplete(self, boolean: bool) -> None: ...
    def getComponent(self) -> java.awt.Component: ...
    def getDropTarget(self) -> 'DropTarget': ...
    def removeNotify(self) -> None: ...

class DropTargetEvent(java.util.EventObject):
    def __init__(self, dropTargetContext: DropTargetContext): ...
    def getDropTargetContext(self) -> DropTargetContext: ...

class DropTargetListener(java.util.EventListener):
    def dragEnter(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def dragExit(self, dropTargetEvent: DropTargetEvent) -> None: ...
    def dragOver(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def drop(self, dropTargetDropEvent: 'DropTargetDropEvent') -> None: ...
    def dropActionChanged(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...

class InvalidDnDOperationException(java.lang.IllegalStateException):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, string: str): ...

class SerializationTester: ...

class DnDEventMulticaster(java.awt.AWTEventMulticaster, DragSourceListener, DragSourceMotionListener):
    @classmethod
    @overload
    def add(cls, dragSourceListener: DragSourceListener, dragSourceListener2: DragSourceListener) -> DragSourceListener: ...
    @classmethod
    @overload
    def add(cls, dragSourceMotionListener: DragSourceMotionListener, dragSourceMotionListener2: DragSourceMotionListener) -> DragSourceMotionListener: ...
    @classmethod
    @overload
    def add(cls, actionListener: java.awt.event.ActionListener, actionListener2: java.awt.event.ActionListener) -> java.awt.event.ActionListener: ...
    @classmethod
    @overload
    def add(cls, adjustmentListener: java.awt.event.AdjustmentListener, adjustmentListener2: java.awt.event.AdjustmentListener) -> java.awt.event.AdjustmentListener: ...
    @classmethod
    @overload
    def add(cls, componentListener: java.awt.event.ComponentListener, componentListener2: java.awt.event.ComponentListener) -> java.awt.event.ComponentListener: ...
    @classmethod
    @overload
    def add(cls, containerListener: java.awt.event.ContainerListener, containerListener2: java.awt.event.ContainerListener) -> java.awt.event.ContainerListener: ...
    @classmethod
    @overload
    def add(cls, focusListener: java.awt.event.FocusListener, focusListener2: java.awt.event.FocusListener) -> java.awt.event.FocusListener: ...
    @classmethod
    @overload
    def add(cls, hierarchyBoundsListener: java.awt.event.HierarchyBoundsListener, hierarchyBoundsListener2: java.awt.event.HierarchyBoundsListener) -> java.awt.event.HierarchyBoundsListener: ...
    @classmethod
    @overload
    def add(cls, hierarchyListener: java.awt.event.HierarchyListener, hierarchyListener2: java.awt.event.HierarchyListener) -> java.awt.event.HierarchyListener: ...
    @classmethod
    @overload
    def add(cls, inputMethodListener: java.awt.event.InputMethodListener, inputMethodListener2: java.awt.event.InputMethodListener) -> java.awt.event.InputMethodListener: ...
    @classmethod
    @overload
    def add(cls, itemListener: java.awt.event.ItemListener, itemListener2: java.awt.event.ItemListener) -> java.awt.event.ItemListener: ...
    @classmethod
    @overload
    def add(cls, keyListener: java.awt.event.KeyListener, keyListener2: java.awt.event.KeyListener) -> java.awt.event.KeyListener: ...
    @classmethod
    @overload
    def add(cls, mouseListener: java.awt.event.MouseListener, mouseListener2: java.awt.event.MouseListener) -> java.awt.event.MouseListener: ...
    @classmethod
    @overload
    def add(cls, mouseMotionListener: java.awt.event.MouseMotionListener, mouseMotionListener2: java.awt.event.MouseMotionListener) -> java.awt.event.MouseMotionListener: ...
    @classmethod
    @overload
    def add(cls, mouseWheelListener: java.awt.event.MouseWheelListener, mouseWheelListener2: java.awt.event.MouseWheelListener) -> java.awt.event.MouseWheelListener: ...
    @classmethod
    @overload
    def add(cls, textListener: java.awt.event.TextListener, textListener2: java.awt.event.TextListener) -> java.awt.event.TextListener: ...
    @classmethod
    @overload
    def add(cls, windowFocusListener: java.awt.event.WindowFocusListener, windowFocusListener2: java.awt.event.WindowFocusListener) -> java.awt.event.WindowFocusListener: ...
    @classmethod
    @overload
    def add(cls, windowListener: java.awt.event.WindowListener, windowListener2: java.awt.event.WindowListener) -> java.awt.event.WindowListener: ...
    @classmethod
    @overload
    def add(cls, windowStateListener: java.awt.event.WindowStateListener, windowStateListener2: java.awt.event.WindowStateListener) -> java.awt.event.WindowStateListener: ...
    def dragDropEnd(self, dragSourceDropEvent: 'DragSourceDropEvent') -> None: ...
    def dragEnter(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragExit(self, dragSourceEvent: DragSourceEvent) -> None: ...
    def dragMouseMoved(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragOver(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dropActionChanged(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    @classmethod
    @overload
    def remove(cls, dragSourceListener: DragSourceListener, dragSourceListener2: DragSourceListener) -> DragSourceListener: ...
    @classmethod
    @overload
    def remove(cls, dragSourceMotionListener: DragSourceMotionListener, dragSourceMotionListener2: DragSourceMotionListener) -> DragSourceMotionListener: ...
    @classmethod
    @overload
    def remove(cls, actionListener: java.awt.event.ActionListener, actionListener2: java.awt.event.ActionListener) -> java.awt.event.ActionListener: ...
    @classmethod
    @overload
    def remove(cls, adjustmentListener: java.awt.event.AdjustmentListener, adjustmentListener2: java.awt.event.AdjustmentListener) -> java.awt.event.AdjustmentListener: ...
    @classmethod
    @overload
    def remove(cls, componentListener: java.awt.event.ComponentListener, componentListener2: java.awt.event.ComponentListener) -> java.awt.event.ComponentListener: ...
    @classmethod
    @overload
    def remove(cls, containerListener: java.awt.event.ContainerListener, containerListener2: java.awt.event.ContainerListener) -> java.awt.event.ContainerListener: ...
    @classmethod
    @overload
    def remove(cls, focusListener: java.awt.event.FocusListener, focusListener2: java.awt.event.FocusListener) -> java.awt.event.FocusListener: ...
    @classmethod
    @overload
    def remove(cls, hierarchyBoundsListener: java.awt.event.HierarchyBoundsListener, hierarchyBoundsListener2: java.awt.event.HierarchyBoundsListener) -> java.awt.event.HierarchyBoundsListener: ...
    @classmethod
    @overload
    def remove(cls, hierarchyListener: java.awt.event.HierarchyListener, hierarchyListener2: java.awt.event.HierarchyListener) -> java.awt.event.HierarchyListener: ...
    @classmethod
    @overload
    def remove(cls, inputMethodListener: java.awt.event.InputMethodListener, inputMethodListener2: java.awt.event.InputMethodListener) -> java.awt.event.InputMethodListener: ...
    @classmethod
    @overload
    def remove(cls, itemListener: java.awt.event.ItemListener, itemListener2: java.awt.event.ItemListener) -> java.awt.event.ItemListener: ...
    @classmethod
    @overload
    def remove(cls, keyListener: java.awt.event.KeyListener, keyListener2: java.awt.event.KeyListener) -> java.awt.event.KeyListener: ...
    @classmethod
    @overload
    def remove(cls, mouseListener: java.awt.event.MouseListener, mouseListener2: java.awt.event.MouseListener) -> java.awt.event.MouseListener: ...
    @classmethod
    @overload
    def remove(cls, mouseMotionListener: java.awt.event.MouseMotionListener, mouseMotionListener2: java.awt.event.MouseMotionListener) -> java.awt.event.MouseMotionListener: ...
    @classmethod
    @overload
    def remove(cls, mouseWheelListener: java.awt.event.MouseWheelListener, mouseWheelListener2: java.awt.event.MouseWheelListener) -> java.awt.event.MouseWheelListener: ...
    @classmethod
    @overload
    def remove(cls, textListener: java.awt.event.TextListener, textListener2: java.awt.event.TextListener) -> java.awt.event.TextListener: ...
    @classmethod
    @overload
    def remove(cls, windowFocusListener: java.awt.event.WindowFocusListener, windowFocusListener2: java.awt.event.WindowFocusListener) -> java.awt.event.WindowFocusListener: ...
    @classmethod
    @overload
    def remove(cls, windowListener: java.awt.event.WindowListener, windowListener2: java.awt.event.WindowListener) -> java.awt.event.WindowListener: ...
    @classmethod
    @overload
    def remove(cls, windowStateListener: java.awt.event.WindowStateListener, windowStateListener2: java.awt.event.WindowStateListener) -> java.awt.event.WindowStateListener: ...

class DragSourceAdapter(DragSourceListener, DragSourceMotionListener):
    def __init__(self): ...
    def dragDropEnd(self, dragSourceDropEvent: 'DragSourceDropEvent') -> None: ...
    def dragEnter(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragExit(self, dragSourceEvent: DragSourceEvent) -> None: ...
    def dragMouseMoved(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragOver(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dropActionChanged(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...

class DragSourceContext(DragSourceListener, DragSourceMotionListener, java.io.Serializable):
    def __init__(self, dragSourceContextPeer: java.awt.dnd.peer.DragSourceContextPeer, dragGestureEvent: DragGestureEvent, cursor: java.awt.Cursor, image: java.awt.Image, point: java.awt.Point, transferable: java.awt.datatransfer.Transferable, dragSourceListener: DragSourceListener): ...
    def addDragSourceListener(self, dragSourceListener: DragSourceListener) -> None: ...
    def dragDropEnd(self, dragSourceDropEvent: 'DragSourceDropEvent') -> None: ...
    def dragEnter(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragExit(self, dragSourceEvent: DragSourceEvent) -> None: ...
    def dragMouseMoved(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dragOver(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def dropActionChanged(self, dragSourceDragEvent: 'DragSourceDragEvent') -> None: ...
    def getComponent(self) -> java.awt.Component: ...
    def getCursor(self) -> java.awt.Cursor: ...
    def getDragSource(self) -> DragSource: ...
    def getSourceActions(self) -> int: ...
    def getTransferable(self) -> java.awt.datatransfer.Transferable: ...
    def getTrigger(self) -> DragGestureEvent: ...
    def removeDragSourceListener(self, dragSourceListener: DragSourceListener) -> None: ...
    def setCursor(self, cursor: java.awt.Cursor) -> None: ...
    def transferablesFlavorsChanged(self) -> None: ...

class DragSourceDragEvent(DragSourceEvent):
    @overload
    def __init__(self, dragSourceContext: DragSourceContext, int: int, int2: int, int3: int): ...
    @overload
    def __init__(self, dragSourceContext: DragSourceContext, int: int, int2: int, int3: int, int4: int, int5: int): ...
    def getDropAction(self) -> int: ...
    def getGestureModifiers(self) -> int: ...
    def getGestureModifiersEx(self) -> int: ...
    def getTargetActions(self) -> int: ...
    def getUserAction(self) -> int: ...

class DragSourceDropEvent(DragSourceEvent):
    @overload
    def __init__(self, dragSourceContext: DragSourceContext): ...
    @overload
    def __init__(self, dragSourceContext: DragSourceContext, int: int, boolean: bool): ...
    @overload
    def __init__(self, dragSourceContext: DragSourceContext, int: int, boolean: bool, int2: int, int3: int): ...
    def getDropAction(self) -> int: ...
    def getDropSuccess(self) -> bool: ...

class DropTarget(DropTargetListener, java.io.Serializable):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, component: java.awt.Component, int: int, dropTargetListener: DropTargetListener): ...
    @overload
    def __init__(self, component: java.awt.Component, int: int, dropTargetListener: DropTargetListener, boolean: bool): ...
    @overload
    def __init__(self, component: java.awt.Component, int: int, dropTargetListener: DropTargetListener, boolean: bool, flavorMap: java.awt.datatransfer.FlavorMap): ...
    @overload
    def __init__(self, component: java.awt.Component, dropTargetListener: DropTargetListener): ...
    def addDropTargetListener(self, dropTargetListener: DropTargetListener) -> None: ...
    def addNotify(self, componentPeer: java.awt.peer.ComponentPeer) -> None: ...
    def dragEnter(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def dragExit(self, dropTargetEvent: DropTargetEvent) -> None: ...
    def dragOver(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def drop(self, dropTargetDropEvent: 'DropTargetDropEvent') -> None: ...
    def dropActionChanged(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def getComponent(self) -> java.awt.Component: ...
    def getDefaultActions(self) -> int: ...
    def getDropTargetContext(self) -> DropTargetContext: ...
    def getFlavorMap(self) -> java.awt.datatransfer.FlavorMap: ...
    def isActive(self) -> bool: ...
    def removeDropTargetListener(self, dropTargetListener: DropTargetListener) -> None: ...
    def removeNotify(self, componentPeer: java.awt.peer.ComponentPeer) -> None: ...
    def setActive(self, boolean: bool) -> None: ...
    def setComponent(self, component: java.awt.Component) -> None: ...
    def setDefaultActions(self, int: int) -> None: ...
    def setFlavorMap(self, flavorMap: java.awt.datatransfer.FlavorMap) -> None: ...

class DropTargetAdapter(DropTargetListener):
    def __init__(self): ...
    def dragEnter(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def dragExit(self, dropTargetEvent: DropTargetEvent) -> None: ...
    def dragOver(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...
    def dropActionChanged(self, dropTargetDragEvent: 'DropTargetDragEvent') -> None: ...

class DropTargetDragEvent(DropTargetEvent):
    def __init__(self, dropTargetContext: DropTargetContext, point: java.awt.Point, int: int, int2: int): ...
    def acceptDrag(self, int: int) -> None: ...
    def getCurrentDataFlavors(self) -> _py_List[java.awt.datatransfer.DataFlavor]: ...
    def getCurrentDataFlavorsAsList(self) -> java.util.List[java.awt.datatransfer.DataFlavor]: ...
    def getDropAction(self) -> int: ...
    def getLocation(self) -> java.awt.Point: ...
    def getSourceActions(self) -> int: ...
    def getTransferable(self) -> java.awt.datatransfer.Transferable: ...
    def isDataFlavorSupported(self, dataFlavor: java.awt.datatransfer.DataFlavor) -> bool: ...
    def rejectDrag(self) -> None: ...

class DropTargetDropEvent(DropTargetEvent):
    @overload
    def __init__(self, dropTargetContext: DropTargetContext, point: java.awt.Point, int: int, int2: int): ...
    @overload
    def __init__(self, dropTargetContext: DropTargetContext, point: java.awt.Point, int: int, int2: int, boolean: bool): ...
    def acceptDrop(self, int: int) -> None: ...
    def dropComplete(self, boolean: bool) -> None: ...
    def getCurrentDataFlavors(self) -> _py_List[java.awt.datatransfer.DataFlavor]: ...
    def getCurrentDataFlavorsAsList(self) -> java.util.List[java.awt.datatransfer.DataFlavor]: ...
    def getDropAction(self) -> int: ...
    def getLocation(self) -> java.awt.Point: ...
    def getSourceActions(self) -> int: ...
    def getTransferable(self) -> java.awt.datatransfer.Transferable: ...
    def isDataFlavorSupported(self, dataFlavor: java.awt.datatransfer.DataFlavor) -> bool: ...
    def isLocalTransfer(self) -> bool: ...
    def rejectDrop(self) -> None: ...

class MouseDragGestureRecognizer(DragGestureRecognizer, java.awt.event.MouseListener, java.awt.event.MouseMotionListener):
    def mouseClicked(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
    def mouseDragged(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
    def mouseEntered(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
    def mouseExited(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
    def mouseMoved(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
    def mousePressed(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
    def mouseReleased(self, mouseEvent: java.awt.event.MouseEvent) -> None: ...
