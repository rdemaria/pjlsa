from typing import Any as _py_Any
from typing import List as _py_List
from typing import TypeVar as _py_TypeVar
from typing import Type as _py_Type
from typing import ClassVar as _py_ClassVar
from typing import Generic as _py_Generic
from typing import overload
import java.applet
import java.awt
import java.beans.beancontext
import java.io
import java.lang
import java.lang.annotation
import java.lang.reflect
import java.net
import java.util
import org.xml.sax
import org.xml.sax.helpers


class AppletInitializer:
    def activate(self, applet: java.applet.Applet) -> None: ...
    def initialize(self, applet: java.applet.Applet, beanContext: java.beans.beancontext.BeanContext) -> None: ...

class BeanInfo:
    ICON_COLOR_16x16: _py_ClassVar[int] = ...
    ICON_COLOR_32x32: _py_ClassVar[int] = ...
    ICON_MONO_16x16: _py_ClassVar[int] = ...
    ICON_MONO_32x32: _py_ClassVar[int] = ...
    def getAdditionalBeanInfo(self) -> _py_List['BeanInfo']: ...
    def getBeanDescriptor(self) -> 'BeanDescriptor': ...
    def getDefaultEventIndex(self) -> int: ...
    def getDefaultPropertyIndex(self) -> int: ...
    def getEventSetDescriptors(self) -> _py_List['EventSetDescriptor']: ...
    def getIcon(self, int: int) -> java.awt.Image: ...
    def getMethodDescriptors(self) -> _py_List['MethodDescriptor']: ...
    def getPropertyDescriptors(self) -> _py_List['PropertyDescriptor']: ...

class Beans:
    def __init__(self): ...
    @classmethod
    def getInstanceOf(cls, object: _py_Any, class_: _py_Type[_py_Any]) -> _py_Any: ...
    @classmethod
    @overload
    def instantiate(cls, classLoader: java.lang.ClassLoader, string: str) -> _py_Any: ...
    @classmethod
    @overload
    def instantiate(cls, classLoader: java.lang.ClassLoader, string: str, beanContext: java.beans.beancontext.BeanContext) -> _py_Any: ...
    @classmethod
    @overload
    def instantiate(cls, classLoader: java.lang.ClassLoader, string: str, beanContext: java.beans.beancontext.BeanContext, appletInitializer: AppletInitializer) -> _py_Any: ...
    @classmethod
    def isDesignTime(cls) -> bool: ...
    @classmethod
    def isGuiAvailable(cls) -> bool: ...
    @classmethod
    def isInstanceOf(cls, object: _py_Any, class_: _py_Type[_py_Any]) -> bool: ...
    @classmethod
    def setDesignTime(cls, boolean: bool) -> None: ...
    @classmethod
    def setGuiAvailable(cls, boolean: bool) -> None: ...

class BeansAppletContext(java.applet.AppletContext):
    def getApplet(self, string: str) -> java.applet.Applet: ...
    def getApplets(self) -> java.util.Enumeration[java.applet.Applet]: ...
    def getAudioClip(self, uRL: java.net.URL) -> java.applet.AudioClip: ...
    def getImage(self, uRL: java.net.URL) -> java.awt.Image: ...
    def getStream(self, string: str) -> java.io.InputStream: ...
    def getStreamKeys(self) -> java.util.Iterator[str]: ...
    def setStream(self, string: str, inputStream: java.io.InputStream) -> None: ...
    @overload
    def showDocument(self, uRL: java.net.URL) -> None: ...
    @overload
    def showDocument(self, uRL: java.net.URL, string: str) -> None: ...
    def showStatus(self, string: str) -> None: ...

class BeansAppletStub(java.applet.AppletStub):
    def appletResize(self, int: int, int2: int) -> None: ...
    def getAppletContext(self) -> java.applet.AppletContext: ...
    def getCodeBase(self) -> java.net.URL: ...
    def getDocumentBase(self) -> java.net.URL: ...
    def getParameter(self, string: str) -> str: ...
    def isActive(self) -> bool: ...

_ChangeListenerMap__L = _py_TypeVar('_ChangeListenerMap__L', bound=java.util.EventListener)  # <L>
class ChangeListenerMap(_py_Generic[_ChangeListenerMap__L]):
    def add(self, string: str, l: _ChangeListenerMap__L) -> None: ...
    def extract(self, l: _ChangeListenerMap__L) -> _ChangeListenerMap__L: ...
    def get(self, string: str) -> _py_List[_ChangeListenerMap__L]: ...
    def getEntries(self) -> java.util.Set[java.util.Map.Entry[str, _py_List[_ChangeListenerMap__L]]]: ...
    @overload
    def getListeners(self, string: str) -> _py_List[_ChangeListenerMap__L]: ...
    @overload
    def getListeners(self) -> _py_List[_ChangeListenerMap__L]: ...
    def hasListeners(self, string: str) -> bool: ...
    def remove(self, string: str, l: _ChangeListenerMap__L) -> None: ...
    def set(self, string: str, lArray: _py_List[_ChangeListenerMap__L]) -> None: ...

class ConstructorProperties(java.lang.annotation.Annotation):
    def equals(self, object: _py_Any) -> bool: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def value(self) -> _py_List[str]: ...

class Customizer:
    def addPropertyChangeListener(self, propertyChangeListener: 'PropertyChangeListener') -> None: ...
    def removePropertyChangeListener(self, propertyChangeListener: 'PropertyChangeListener') -> None: ...
    def setObject(self, object: _py_Any) -> None: ...

class DesignMode:
    PROPERTYNAME: _py_ClassVar[str] = ...
    def isDesignTime(self) -> bool: ...
    def setDesignTime(self, boolean: bool) -> None: ...

class Encoder:
    def __init__(self): ...
    def get(self, object: _py_Any) -> _py_Any: ...
    def getExceptionListener(self) -> 'ExceptionListener': ...
    def getPersistenceDelegate(self, class_: _py_Type[_py_Any]) -> 'PersistenceDelegate': ...
    def remove(self, object: _py_Any) -> _py_Any: ...
    def setExceptionListener(self, exceptionListener: 'ExceptionListener') -> None: ...
    def setPersistenceDelegate(self, class_: _py_Type[_py_Any], persistenceDelegate: 'PersistenceDelegate') -> None: ...
    def writeExpression(self, expression: 'Expression') -> None: ...
    def writeStatement(self, statement: 'Statement') -> None: ...

class EventHandler(java.lang.reflect.InvocationHandler):
    def __init__(self, object: _py_Any, string: str, string2: str, string3: str): ...
    _create_0__T = _py_TypeVar('_create_0__T')  # <T>
    @classmethod
    @overload
    def create(cls, class_: _py_Type[_create_0__T], object: _py_Any, string: str) -> _create_0__T: ...
    _create_1__T = _py_TypeVar('_create_1__T')  # <T>
    @classmethod
    @overload
    def create(cls, class_: _py_Type[_create_1__T], object: _py_Any, string: str, string2: str) -> _create_1__T: ...
    _create_2__T = _py_TypeVar('_create_2__T')  # <T>
    @classmethod
    @overload
    def create(cls, class_: _py_Type[_create_2__T], object: _py_Any, string: str, string2: str, string3: str) -> _create_2__T: ...
    def getAction(self) -> str: ...
    def getEventPropertyName(self) -> str: ...
    def getListenerMethodName(self) -> str: ...
    def getTarget(self) -> _py_Any: ...
    def invoke(self, object: _py_Any, method: java.lang.reflect.Method, objectArray: _py_List[_py_Any]) -> _py_Any: ...

class ExceptionListener:
    def exceptionThrown(self, exception: java.lang.Exception) -> None: ...

class FeatureDescriptor:
    def __init__(self): ...
    def attributeNames(self) -> java.util.Enumeration[str]: ...
    def getDisplayName(self) -> str: ...
    def getName(self) -> str: ...
    def getShortDescription(self) -> str: ...
    def getValue(self, string: str) -> _py_Any: ...
    def isExpert(self) -> bool: ...
    def isHidden(self) -> bool: ...
    def isPreferred(self) -> bool: ...
    def setDisplayName(self, string: str) -> None: ...
    def setExpert(self, boolean: bool) -> None: ...
    def setHidden(self, boolean: bool) -> None: ...
    def setName(self, string: str) -> None: ...
    def setPreferred(self, boolean: bool) -> None: ...
    def setShortDescription(self, string: str) -> None: ...
    def setValue(self, string: str, object: _py_Any) -> None: ...
    def toString(self) -> str: ...

class IntrospectionException(java.lang.Exception):
    def __init__(self, string: str): ...

class Introspector:
    USE_ALL_BEANINFO: _py_ClassVar[int] = ...
    IGNORE_IMMEDIATE_BEANINFO: _py_ClassVar[int] = ...
    IGNORE_ALL_BEANINFO: _py_ClassVar[int] = ...
    @classmethod
    def decapitalize(cls, string: str) -> str: ...
    @classmethod
    def flushCaches(cls) -> None: ...
    @classmethod
    def flushFromCaches(cls, class_: _py_Type[_py_Any]) -> None: ...
    @classmethod
    @overload
    def getBeanInfo(cls, class_: _py_Type[_py_Any]) -> BeanInfo: ...
    @classmethod
    @overload
    def getBeanInfo(cls, class_: _py_Type[_py_Any], int: int) -> BeanInfo: ...
    @classmethod
    @overload
    def getBeanInfo(cls, class_: _py_Type[_py_Any], class2: _py_Type[_py_Any]) -> BeanInfo: ...
    @classmethod
    @overload
    def getBeanInfo(cls, class_: _py_Type[_py_Any], class2: _py_Type[_py_Any], int: int) -> BeanInfo: ...
    @classmethod
    def getBeanInfoSearchPath(cls) -> _py_List[str]: ...
    @classmethod
    def setBeanInfoSearchPath(cls, stringArray: _py_List[str]) -> None: ...

class MetaData:
    @classmethod
    def getPersistenceDelegate(cls, class_: _py_Type) -> 'PersistenceDelegate': ...

class MethodRef: ...

class NameGenerator:
    def __init__(self): ...
    @classmethod
    def capitalize(cls, string: str) -> str: ...
    def clear(self) -> None: ...
    def instanceName(self, object: _py_Any) -> str: ...
    @classmethod
    def unqualifiedClassName(cls, class_: _py_Type) -> str: ...

class ObjectInputStreamWithLoader(java.io.ObjectInputStream):
    def __init__(self, inputStream: java.io.InputStream, classLoader: java.lang.ClassLoader): ...

class PersistenceDelegate:
    def __init__(self): ...
    def writeObject(self, object: _py_Any, encoder: Encoder) -> None: ...

class PropertyChangeEvent(java.util.EventObject):
    def __init__(self, object: _py_Any, string: str, object2: _py_Any, object3: _py_Any): ...
    def getNewValue(self) -> _py_Any: ...
    def getOldValue(self) -> _py_Any: ...
    def getPropagationId(self) -> _py_Any: ...
    def getPropertyName(self) -> str: ...
    def setPropagationId(self, object: _py_Any) -> None: ...
    def toString(self) -> str: ...

class PropertyChangeListener(java.util.EventListener):
    def propertyChange(self, propertyChangeEvent: PropertyChangeEvent) -> None: ...

class PropertyChangeSupport(java.io.Serializable):
    def __init__(self, object: _py_Any): ...
    @overload
    def addPropertyChangeListener(self, propertyChangeListener: PropertyChangeListener) -> None: ...
    @overload
    def addPropertyChangeListener(self, string: str, propertyChangeListener: PropertyChangeListener) -> None: ...
    @overload
    def fireIndexedPropertyChange(self, string: str, int: int, boolean: bool, boolean2: bool) -> None: ...
    @overload
    def fireIndexedPropertyChange(self, string: str, int: int, int2: int, int3: int) -> None: ...
    @overload
    def fireIndexedPropertyChange(self, string: str, int: int, object: _py_Any, object2: _py_Any) -> None: ...
    @overload
    def firePropertyChange(self, propertyChangeEvent: PropertyChangeEvent) -> None: ...
    @overload
    def firePropertyChange(self, string: str, boolean: bool, boolean2: bool) -> None: ...
    @overload
    def firePropertyChange(self, string: str, int: int, int2: int) -> None: ...
    @overload
    def firePropertyChange(self, string: str, object: _py_Any, object2: _py_Any) -> None: ...
    @overload
    def getPropertyChangeListeners(self) -> _py_List[PropertyChangeListener]: ...
    @overload
    def getPropertyChangeListeners(self, string: str) -> _py_List[PropertyChangeListener]: ...
    def hasListeners(self, string: str) -> bool: ...
    @overload
    def removePropertyChangeListener(self, propertyChangeListener: PropertyChangeListener) -> None: ...
    @overload
    def removePropertyChangeListener(self, string: str, propertyChangeListener: PropertyChangeListener) -> None: ...

class PropertyEditor:
    def addPropertyChangeListener(self, propertyChangeListener: PropertyChangeListener) -> None: ...
    def getAsText(self) -> str: ...
    def getCustomEditor(self) -> java.awt.Component: ...
    def getJavaInitializationString(self) -> str: ...
    def getTags(self) -> _py_List[str]: ...
    def getValue(self) -> _py_Any: ...
    def isPaintable(self) -> bool: ...
    def paintValue(self, graphics: java.awt.Graphics, rectangle: java.awt.Rectangle) -> None: ...
    def removePropertyChangeListener(self, propertyChangeListener: PropertyChangeListener) -> None: ...
    def setAsText(self, string: str) -> None: ...
    def setValue(self, object: _py_Any) -> None: ...
    def supportsCustomEditor(self) -> bool: ...

class PropertyEditorManager:
    def __init__(self): ...
    @classmethod
    def findEditor(cls, class_: _py_Type[_py_Any]) -> PropertyEditor: ...
    @classmethod
    def getEditorSearchPath(cls) -> _py_List[str]: ...
    @classmethod
    def registerEditor(cls, class_: _py_Type[_py_Any], class2: _py_Type[_py_Any]) -> None: ...
    @classmethod
    def setEditorSearchPath(cls, stringArray: _py_List[str]) -> None: ...

class PropertyVetoException(java.lang.Exception):
    def __init__(self, string: str, propertyChangeEvent: PropertyChangeEvent): ...
    def getPropertyChangeEvent(self) -> PropertyChangeEvent: ...

class Statement:
    def __init__(self, object: _py_Any, string: str, objectArray: _py_List[_py_Any]): ...
    def execute(self) -> None: ...
    def getArguments(self) -> _py_List[_py_Any]: ...
    def getMethodName(self) -> str: ...
    def getTarget(self) -> _py_Any: ...
    def toString(self) -> str: ...

class ThreadGroupContext: ...

class Transient(java.lang.annotation.Annotation):
    def equals(self, object: _py_Any) -> bool: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def value(self) -> bool: ...

class VetoableChangeListener(java.util.EventListener):
    def vetoableChange(self, propertyChangeEvent: PropertyChangeEvent) -> None: ...

class VetoableChangeSupport(java.io.Serializable):
    def __init__(self, object: _py_Any): ...
    @overload
    def addVetoableChangeListener(self, vetoableChangeListener: VetoableChangeListener) -> None: ...
    @overload
    def addVetoableChangeListener(self, string: str, vetoableChangeListener: VetoableChangeListener) -> None: ...
    @overload
    def fireVetoableChange(self, propertyChangeEvent: PropertyChangeEvent) -> None: ...
    @overload
    def fireVetoableChange(self, string: str, boolean: bool, boolean2: bool) -> None: ...
    @overload
    def fireVetoableChange(self, string: str, int: int, int2: int) -> None: ...
    @overload
    def fireVetoableChange(self, string: str, object: _py_Any, object2: _py_Any) -> None: ...
    @overload
    def getVetoableChangeListeners(self) -> _py_List[VetoableChangeListener]: ...
    @overload
    def getVetoableChangeListeners(self, string: str) -> _py_List[VetoableChangeListener]: ...
    def hasListeners(self, string: str) -> bool: ...
    @overload
    def removeVetoableChangeListener(self, vetoableChangeListener: VetoableChangeListener) -> None: ...
    @overload
    def removeVetoableChangeListener(self, string: str, vetoableChangeListener: VetoableChangeListener) -> None: ...

class Visibility:
    def avoidingGui(self) -> bool: ...
    def dontUseGui(self) -> None: ...
    def needsGui(self) -> bool: ...
    def okToUseGui(self) -> None: ...

_WeakIdentityMap__T = _py_TypeVar('_WeakIdentityMap__T')  # <T>
class WeakIdentityMap(_py_Generic[_WeakIdentityMap__T]):
    def get(self, object: _py_Any) -> _WeakIdentityMap__T: ...

class XMLDecoder(java.lang.AutoCloseable):
    @overload
    def __init__(self, inputStream: java.io.InputStream): ...
    @overload
    def __init__(self, inputStream: java.io.InputStream, object: _py_Any): ...
    @overload
    def __init__(self, inputStream: java.io.InputStream, object: _py_Any, exceptionListener: ExceptionListener): ...
    @overload
    def __init__(self, inputStream: java.io.InputStream, object: _py_Any, exceptionListener: ExceptionListener, classLoader: java.lang.ClassLoader): ...
    @overload
    def __init__(self, inputSource: org.xml.sax.InputSource): ...
    def close(self) -> None: ...
    @classmethod
    def createHandler(cls, object: _py_Any, exceptionListener: ExceptionListener, classLoader: java.lang.ClassLoader) -> org.xml.sax.helpers.DefaultHandler: ...
    def getExceptionListener(self) -> ExceptionListener: ...
    def getOwner(self) -> _py_Any: ...
    def readObject(self) -> _py_Any: ...
    def setExceptionListener(self, exceptionListener: ExceptionListener) -> None: ...
    def setOwner(self, object: _py_Any) -> None: ...

class BeanDescriptor(FeatureDescriptor):
    @overload
    def __init__(self, class_: _py_Type[_py_Any]): ...
    @overload
    def __init__(self, class_: _py_Type[_py_Any], class2: _py_Type[_py_Any]): ...
    def getBeanClass(self) -> _py_Type[_py_Any]: ...
    def getCustomizerClass(self) -> _py_Type[_py_Any]: ...

class DefaultPersistenceDelegate(PersistenceDelegate):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, stringArray: _py_List[str]): ...

class EventSetDescriptor(FeatureDescriptor):
    @overload
    def __init__(self, class_: _py_Type[_py_Any], string: str, class2: _py_Type[_py_Any], string2: str): ...
    @overload
    def __init__(self, class_: _py_Type[_py_Any], string: str, class2: _py_Type[_py_Any], stringArray: _py_List[str], string3: str, string4: str): ...
    @overload
    def __init__(self, class_: _py_Type[_py_Any], string: str, class2: _py_Type[_py_Any], stringArray: _py_List[str], string3: str, string4: str, string5: str): ...
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any], methodDescriptorArray: _py_List['MethodDescriptor'], method2: java.lang.reflect.Method, method3: java.lang.reflect.Method): ...
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any], methodArray: _py_List[java.lang.reflect.Method], method2: java.lang.reflect.Method, method3: java.lang.reflect.Method): ...
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any], methodArray: _py_List[java.lang.reflect.Method], method2: java.lang.reflect.Method, method3: java.lang.reflect.Method, method4: java.lang.reflect.Method): ...
    def getAddListenerMethod(self) -> java.lang.reflect.Method: ...
    def getGetListenerMethod(self) -> java.lang.reflect.Method: ...
    def getListenerMethodDescriptors(self) -> _py_List['MethodDescriptor']: ...
    def getListenerMethods(self) -> _py_List[java.lang.reflect.Method]: ...
    def getListenerType(self) -> _py_Type[_py_Any]: ...
    def getRemoveListenerMethod(self) -> java.lang.reflect.Method: ...
    def isInDefaultEventSet(self) -> bool: ...
    def isUnicast(self) -> bool: ...
    def setInDefaultEventSet(self, boolean: bool) -> None: ...
    def setUnicast(self, boolean: bool) -> None: ...

class Expression(Statement):
    @overload
    def __init__(self, object: _py_Any, object2: _py_Any, string: str, objectArray: _py_List[_py_Any]): ...
    @overload
    def __init__(self, object: _py_Any, string: str, objectArray: _py_List[_py_Any]): ...
    def execute(self) -> None: ...
    def getValue(self) -> _py_Any: ...
    def setValue(self, object: _py_Any) -> None: ...
    def toString(self) -> str: ...

class IndexedPropertyChangeEvent(PropertyChangeEvent):
    def __init__(self, object: _py_Any, string: str, object2: _py_Any, object3: _py_Any, int: int): ...
    def getIndex(self) -> int: ...

class MethodDescriptor(FeatureDescriptor):
    @overload
    def __init__(self, method: java.lang.reflect.Method): ...
    @overload
    def __init__(self, method: java.lang.reflect.Method, parameterDescriptorArray: _py_List['ParameterDescriptor']): ...
    def getMethod(self) -> java.lang.reflect.Method: ...
    def getParameterDescriptors(self) -> _py_List['ParameterDescriptor']: ...

class ParameterDescriptor(FeatureDescriptor):
    def __init__(self): ...

class PropertyChangeListenerProxy(java.util.EventListenerProxy[PropertyChangeListener], PropertyChangeListener):
    def __init__(self, string: str, propertyChangeListener: PropertyChangeListener): ...
    def getPropertyName(self) -> str: ...
    def propertyChange(self, propertyChangeEvent: PropertyChangeEvent) -> None: ...

class PropertyDescriptor(FeatureDescriptor):
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any]): ...
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any], string2: str, string3: str): ...
    @overload
    def __init__(self, string: str, method: java.lang.reflect.Method, method2: java.lang.reflect.Method): ...
    def createPropertyEditor(self, object: _py_Any) -> PropertyEditor: ...
    def equals(self, object: _py_Any) -> bool: ...
    def getPropertyEditorClass(self) -> _py_Type[_py_Any]: ...
    def getPropertyType(self) -> _py_Type[_py_Any]: ...
    def getReadMethod(self) -> java.lang.reflect.Method: ...
    def getWriteMethod(self) -> java.lang.reflect.Method: ...
    def hashCode(self) -> int: ...
    def isBound(self) -> bool: ...
    def isConstrained(self) -> bool: ...
    def setBound(self, boolean: bool) -> None: ...
    def setConstrained(self, boolean: bool) -> None: ...
    def setPropertyEditorClass(self, class_: _py_Type[_py_Any]) -> None: ...
    def setReadMethod(self, method: java.lang.reflect.Method) -> None: ...
    def setWriteMethod(self, method: java.lang.reflect.Method) -> None: ...

class PropertyEditorSupport(PropertyEditor):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, object: _py_Any): ...
    def addPropertyChangeListener(self, propertyChangeListener: PropertyChangeListener) -> None: ...
    def firePropertyChange(self) -> None: ...
    def getAsText(self) -> str: ...
    def getCustomEditor(self) -> java.awt.Component: ...
    def getJavaInitializationString(self) -> str: ...
    def getSource(self) -> _py_Any: ...
    def getTags(self) -> _py_List[str]: ...
    def getValue(self) -> _py_Any: ...
    def isPaintable(self) -> bool: ...
    def paintValue(self, graphics: java.awt.Graphics, rectangle: java.awt.Rectangle) -> None: ...
    def removePropertyChangeListener(self, propertyChangeListener: PropertyChangeListener) -> None: ...
    def setAsText(self, string: str) -> None: ...
    def setSource(self, object: _py_Any) -> None: ...
    def setValue(self, object: _py_Any) -> None: ...
    def supportsCustomEditor(self) -> bool: ...

class SimpleBeanInfo(BeanInfo):
    def __init__(self): ...
    def getAdditionalBeanInfo(self) -> _py_List[BeanInfo]: ...
    def getBeanDescriptor(self) -> BeanDescriptor: ...
    def getDefaultEventIndex(self) -> int: ...
    def getDefaultPropertyIndex(self) -> int: ...
    def getEventSetDescriptors(self) -> _py_List[EventSetDescriptor]: ...
    def getIcon(self, int: int) -> java.awt.Image: ...
    def getMethodDescriptors(self) -> _py_List[MethodDescriptor]: ...
    def getPropertyDescriptors(self) -> _py_List[PropertyDescriptor]: ...
    def loadImage(self, string: str) -> java.awt.Image: ...

class VetoableChangeListenerProxy(java.util.EventListenerProxy[VetoableChangeListener], VetoableChangeListener):
    def __init__(self, string: str, vetoableChangeListener: VetoableChangeListener): ...
    def getPropertyName(self) -> str: ...
    def vetoableChange(self, propertyChangeEvent: PropertyChangeEvent) -> None: ...

class XMLEncoder(Encoder, java.lang.AutoCloseable):
    @overload
    def __init__(self, outputStream: java.io.OutputStream): ...
    @overload
    def __init__(self, outputStream: java.io.OutputStream, string: str, boolean: bool, int: int): ...
    def close(self) -> None: ...
    def flush(self) -> None: ...
    def getOwner(self) -> _py_Any: ...
    def setOwner(self, object: _py_Any) -> None: ...
    def writeExpression(self, expression: Expression) -> None: ...
    def writeObject(self, object: _py_Any) -> None: ...
    def writeStatement(self, statement: Statement) -> None: ...

class GenericBeanInfo(SimpleBeanInfo):
    def __init__(self, beanDescriptor: BeanDescriptor, eventSetDescriptorArray: _py_List[EventSetDescriptor], int: int, propertyDescriptorArray: _py_List[PropertyDescriptor], int2: int, methodDescriptorArray: _py_List[MethodDescriptor], beanInfo: BeanInfo): ...
    def getBeanDescriptor(self) -> BeanDescriptor: ...
    def getDefaultEventIndex(self) -> int: ...
    def getDefaultPropertyIndex(self) -> int: ...
    def getEventSetDescriptors(self) -> _py_List[EventSetDescriptor]: ...
    def getIcon(self, int: int) -> java.awt.Image: ...
    def getMethodDescriptors(self) -> _py_List[MethodDescriptor]: ...
    def getPropertyDescriptors(self) -> _py_List[PropertyDescriptor]: ...

class IndexedPropertyDescriptor(PropertyDescriptor):
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any]): ...
    @overload
    def __init__(self, string: str, class_: _py_Type[_py_Any], string2: str, string3: str, string4: str, string5: str): ...
    @overload
    def __init__(self, string: str, method: java.lang.reflect.Method, method2: java.lang.reflect.Method, method3: java.lang.reflect.Method, method4: java.lang.reflect.Method): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getIndexedPropertyType(self) -> _py_Type[_py_Any]: ...
    def getIndexedReadMethod(self) -> java.lang.reflect.Method: ...
    def getIndexedWriteMethod(self) -> java.lang.reflect.Method: ...
    def hashCode(self) -> int: ...
    def setIndexedReadMethod(self, method: java.lang.reflect.Method) -> None: ...
    def setIndexedWriteMethod(self, method: java.lang.reflect.Method) -> None: ...
