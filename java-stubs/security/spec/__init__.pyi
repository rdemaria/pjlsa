from typing import Any as _py_Any
from typing import List as _py_List
from typing import ClassVar as _py_ClassVar
from typing import overload
import java.lang
import java.math
import java.security
import java.security.interfaces


class AlgorithmParameterSpec: ...

class ECField:
    def getFieldSize(self) -> int: ...

class ECPoint:
    POINT_INFINITY: _py_ClassVar['ECPoint'] = ...
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getAffineX(self) -> java.math.BigInteger: ...
    def getAffineY(self) -> java.math.BigInteger: ...
    def hashCode(self) -> int: ...

class EllipticCurve:
    @overload
    def __init__(self, eCField: ECField, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger): ...
    @overload
    def __init__(self, eCField: ECField, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, byteArray: _py_List[int]): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getA(self) -> java.math.BigInteger: ...
    def getB(self) -> java.math.BigInteger: ...
    def getField(self) -> ECField: ...
    def getSeed(self) -> _py_List[int]: ...
    def hashCode(self) -> int: ...

class InvalidKeySpecException(java.security.GeneralSecurityException):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, string: str): ...
    @overload
    def __init__(self, string: str, throwable: java.lang.Throwable): ...
    @overload
    def __init__(self, throwable: java.lang.Throwable): ...

class InvalidParameterSpecException(java.security.GeneralSecurityException):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, string: str): ...

class KeySpec: ...

class RSAOtherPrimeInfo:
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, bigInteger3: java.math.BigInteger): ...
    def getCrtCoefficient(self) -> java.math.BigInteger: ...
    def getExponent(self) -> java.math.BigInteger: ...
    def getPrime(self) -> java.math.BigInteger: ...

class DSAGenParameterSpec(AlgorithmParameterSpec):
    @overload
    def __init__(self, int: int, int2: int): ...
    @overload
    def __init__(self, int: int, int2: int, int3: int): ...
    def getPrimePLength(self) -> int: ...
    def getSeedLength(self) -> int: ...
    def getSubprimeQLength(self) -> int: ...

class DSAParameterSpec(AlgorithmParameterSpec, java.security.interfaces.DSAParams):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, bigInteger3: java.math.BigInteger): ...
    def getG(self) -> java.math.BigInteger: ...
    def getP(self) -> java.math.BigInteger: ...
    def getQ(self) -> java.math.BigInteger: ...

class DSAPrivateKeySpec(KeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, bigInteger3: java.math.BigInteger, bigInteger4: java.math.BigInteger): ...
    def getG(self) -> java.math.BigInteger: ...
    def getP(self) -> java.math.BigInteger: ...
    def getQ(self) -> java.math.BigInteger: ...
    def getX(self) -> java.math.BigInteger: ...

class DSAPublicKeySpec(KeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, bigInteger3: java.math.BigInteger, bigInteger4: java.math.BigInteger): ...
    def getG(self) -> java.math.BigInteger: ...
    def getP(self) -> java.math.BigInteger: ...
    def getQ(self) -> java.math.BigInteger: ...
    def getY(self) -> java.math.BigInteger: ...

class ECFieldF2m(ECField):
    @overload
    def __init__(self, int: int): ...
    @overload
    def __init__(self, int: int, intArray: _py_List[int]): ...
    @overload
    def __init__(self, int: int, bigInteger: java.math.BigInteger): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getFieldSize(self) -> int: ...
    def getM(self) -> int: ...
    def getMidTermsOfReductionPolynomial(self) -> _py_List[int]: ...
    def getReductionPolynomial(self) -> java.math.BigInteger: ...
    def hashCode(self) -> int: ...

class ECFieldFp(ECField):
    def __init__(self, bigInteger: java.math.BigInteger): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getFieldSize(self) -> int: ...
    def getP(self) -> java.math.BigInteger: ...
    def hashCode(self) -> int: ...

class ECGenParameterSpec(AlgorithmParameterSpec):
    def __init__(self, string: str): ...
    def getName(self) -> str: ...

class ECParameterSpec(AlgorithmParameterSpec):
    def __init__(self, ellipticCurve: EllipticCurve, eCPoint: ECPoint, bigInteger: java.math.BigInteger, int: int): ...
    def getCofactor(self) -> int: ...
    def getCurve(self) -> EllipticCurve: ...
    def getGenerator(self) -> ECPoint: ...
    def getOrder(self) -> java.math.BigInteger: ...

class ECPrivateKeySpec(KeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, eCParameterSpec: ECParameterSpec): ...
    def getParams(self) -> ECParameterSpec: ...
    def getS(self) -> java.math.BigInteger: ...

class ECPublicKeySpec(KeySpec):
    def __init__(self, eCPoint: ECPoint, eCParameterSpec: ECParameterSpec): ...
    def getParams(self) -> ECParameterSpec: ...
    def getW(self) -> ECPoint: ...

class EncodedKeySpec(KeySpec):
    def __init__(self, byteArray: _py_List[int]): ...
    def getEncoded(self) -> _py_List[int]: ...
    def getFormat(self) -> str: ...

class MGF1ParameterSpec(AlgorithmParameterSpec):
    SHA1: _py_ClassVar['MGF1ParameterSpec'] = ...
    SHA224: _py_ClassVar['MGF1ParameterSpec'] = ...
    SHA256: _py_ClassVar['MGF1ParameterSpec'] = ...
    SHA384: _py_ClassVar['MGF1ParameterSpec'] = ...
    SHA512: _py_ClassVar['MGF1ParameterSpec'] = ...
    def __init__(self, string: str): ...
    def getDigestAlgorithm(self) -> str: ...

class PSSParameterSpec(AlgorithmParameterSpec):
    DEFAULT: _py_ClassVar['PSSParameterSpec'] = ...
    @overload
    def __init__(self, int: int): ...
    @overload
    def __init__(self, string: str, string2: str, algorithmParameterSpec: AlgorithmParameterSpec, int: int, int2: int): ...
    def getDigestAlgorithm(self) -> str: ...
    def getMGFAlgorithm(self) -> str: ...
    def getMGFParameters(self) -> AlgorithmParameterSpec: ...
    def getSaltLength(self) -> int: ...
    def getTrailerField(self) -> int: ...

class RSAKeyGenParameterSpec(AlgorithmParameterSpec):
    F0: _py_ClassVar[java.math.BigInteger] = ...
    F4: _py_ClassVar[java.math.BigInteger] = ...
    def __init__(self, int: int, bigInteger: java.math.BigInteger): ...
    def getKeysize(self) -> int: ...
    def getPublicExponent(self) -> java.math.BigInteger: ...

class RSAPrivateKeySpec(KeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger): ...
    def getModulus(self) -> java.math.BigInteger: ...
    def getPrivateExponent(self) -> java.math.BigInteger: ...

class RSAPublicKeySpec(KeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger): ...
    def getModulus(self) -> java.math.BigInteger: ...
    def getPublicExponent(self) -> java.math.BigInteger: ...

class PKCS8EncodedKeySpec(EncodedKeySpec):
    def __init__(self, byteArray: _py_List[int]): ...
    def getEncoded(self) -> _py_List[int]: ...
    def getFormat(self) -> str: ...

class RSAMultiPrimePrivateCrtKeySpec(RSAPrivateKeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, bigInteger3: java.math.BigInteger, bigInteger4: java.math.BigInteger, bigInteger5: java.math.BigInteger, bigInteger6: java.math.BigInteger, bigInteger7: java.math.BigInteger, bigInteger8: java.math.BigInteger, rSAOtherPrimeInfoArray: _py_List[RSAOtherPrimeInfo]): ...
    def getCrtCoefficient(self) -> java.math.BigInteger: ...
    def getOtherPrimeInfo(self) -> _py_List[RSAOtherPrimeInfo]: ...
    def getPrimeExponentP(self) -> java.math.BigInteger: ...
    def getPrimeExponentQ(self) -> java.math.BigInteger: ...
    def getPrimeP(self) -> java.math.BigInteger: ...
    def getPrimeQ(self) -> java.math.BigInteger: ...
    def getPublicExponent(self) -> java.math.BigInteger: ...

class RSAPrivateCrtKeySpec(RSAPrivateKeySpec):
    def __init__(self, bigInteger: java.math.BigInteger, bigInteger2: java.math.BigInteger, bigInteger3: java.math.BigInteger, bigInteger4: java.math.BigInteger, bigInteger5: java.math.BigInteger, bigInteger6: java.math.BigInteger, bigInteger7: java.math.BigInteger, bigInteger8: java.math.BigInteger): ...
    def getCrtCoefficient(self) -> java.math.BigInteger: ...
    def getPrimeExponentP(self) -> java.math.BigInteger: ...
    def getPrimeExponentQ(self) -> java.math.BigInteger: ...
    def getPrimeP(self) -> java.math.BigInteger: ...
    def getPrimeQ(self) -> java.math.BigInteger: ...
    def getPublicExponent(self) -> java.math.BigInteger: ...

class X509EncodedKeySpec(EncodedKeySpec):
    def __init__(self, byteArray: _py_List[int]): ...
    def getEncoded(self) -> _py_List[int]: ...
    def getFormat(self) -> str: ...
