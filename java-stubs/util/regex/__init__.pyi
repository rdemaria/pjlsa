from typing import List as _py_List
from typing import TypeVar as _py_TypeVar
from typing import Type as _py_Type
from typing import ClassVar as _py_ClassVar
from typing import overload
import java.io
import java.lang
import java.util.function
import java.util.stream


class ASCII: ...

class MatchResult:
    @overload
    def end(self) -> int: ...
    @overload
    def end(self, int: int) -> int: ...
    @overload
    def group(self) -> str: ...
    @overload
    def group(self, int: int) -> str: ...
    def groupCount(self) -> int: ...
    @overload
    def start(self) -> int: ...
    @overload
    def start(self, int: int) -> int: ...

class Pattern(java.io.Serializable):
    UNIX_LINES: _py_ClassVar[int] = ...
    CASE_INSENSITIVE: _py_ClassVar[int] = ...
    COMMENTS: _py_ClassVar[int] = ...
    MULTILINE: _py_ClassVar[int] = ...
    LITERAL: _py_ClassVar[int] = ...
    DOTALL: _py_ClassVar[int] = ...
    UNICODE_CASE: _py_ClassVar[int] = ...
    CANON_EQ: _py_ClassVar[int] = ...
    UNICODE_CHARACTER_CLASS: _py_ClassVar[int] = ...
    def asPredicate(self) -> java.util.function.Predicate[str]: ...
    @classmethod
    @overload
    def compile(cls, string: str) -> 'Pattern': ...
    @classmethod
    @overload
    def compile(cls, string: str, int: int) -> 'Pattern': ...
    def flags(self) -> int: ...
    def matcher(self, charSequence: java.lang.CharSequence) -> 'Matcher': ...
    @classmethod
    def matches(cls, string: str, charSequence: java.lang.CharSequence) -> bool: ...
    def pattern(self) -> str: ...
    @classmethod
    def quote(cls, string: str) -> str: ...
    @overload
    def split(self, charSequence: java.lang.CharSequence) -> _py_List[str]: ...
    @overload
    def split(self, charSequence: java.lang.CharSequence, int: int) -> _py_List[str]: ...
    def splitAsStream(self, charSequence: java.lang.CharSequence) -> java.util.stream.Stream[str]: ...
    def toString(self) -> str: ...

class PatternSyntaxException(java.lang.IllegalArgumentException):
    def __init__(self, string: str, string2: str, int: int): ...
    def getDescription(self) -> str: ...
    def getIndex(self) -> int: ...
    def getMessage(self) -> str: ...
    def getPattern(self) -> str: ...

class UnicodeProp(java.lang.Enum['UnicodeProp']):
    ALPHABETIC: _py_ClassVar['UnicodeProp'] = ...
    LETTER: _py_ClassVar['UnicodeProp'] = ...
    IDEOGRAPHIC: _py_ClassVar['UnicodeProp'] = ...
    LOWERCASE: _py_ClassVar['UnicodeProp'] = ...
    UPPERCASE: _py_ClassVar['UnicodeProp'] = ...
    TITLECASE: _py_ClassVar['UnicodeProp'] = ...
    WHITE_SPACE: _py_ClassVar['UnicodeProp'] = ...
    CONTROL: _py_ClassVar['UnicodeProp'] = ...
    PUNCTUATION: _py_ClassVar['UnicodeProp'] = ...
    HEX_DIGIT: _py_ClassVar['UnicodeProp'] = ...
    ASSIGNED: _py_ClassVar['UnicodeProp'] = ...
    NONCHARACTER_CODE_POINT: _py_ClassVar['UnicodeProp'] = ...
    DIGIT: _py_ClassVar['UnicodeProp'] = ...
    ALNUM: _py_ClassVar['UnicodeProp'] = ...
    BLANK: _py_ClassVar['UnicodeProp'] = ...
    GRAPH: _py_ClassVar['UnicodeProp'] = ...
    PRINT: _py_ClassVar['UnicodeProp'] = ...
    WORD: _py_ClassVar['UnicodeProp'] = ...
    JOIN_CONTROL: _py_ClassVar['UnicodeProp'] = ...
    @classmethod
    def forName(cls, string: str) -> 'UnicodeProp': ...
    @classmethod
    def forPOSIXName(cls, string: str) -> 'UnicodeProp': ...
    _valueOf_0__T = _py_TypeVar('_valueOf_0__T', bound=java.lang.Enum)  # <T>
    @classmethod
    @overload
    def valueOf(cls, class_: _py_Type[_valueOf_0__T], string: str) -> _valueOf_0__T: ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'UnicodeProp': ...
    @classmethod
    def values(cls) -> _py_List['UnicodeProp']: ...

class Matcher(MatchResult):
    def appendReplacement(self, stringBuffer: java.lang.StringBuffer, string2: str) -> 'Matcher': ...
    def appendTail(self, stringBuffer: java.lang.StringBuffer) -> java.lang.StringBuffer: ...
    @overload
    def end(self) -> int: ...
    @overload
    def end(self, int: int) -> int: ...
    @overload
    def end(self, string: str) -> int: ...
    @overload
    def find(self) -> bool: ...
    @overload
    def find(self, int: int) -> bool: ...
    @overload
    def group(self) -> str: ...
    @overload
    def group(self, int: int) -> str: ...
    @overload
    def group(self, string: str) -> str: ...
    def groupCount(self) -> int: ...
    def hasAnchoringBounds(self) -> bool: ...
    def hasTransparentBounds(self) -> bool: ...
    def hitEnd(self) -> bool: ...
    def lookingAt(self) -> bool: ...
    def matches(self) -> bool: ...
    def pattern(self) -> Pattern: ...
    @classmethod
    def quoteReplacement(cls, string: str) -> str: ...
    def region(self, int: int, int2: int) -> 'Matcher': ...
    def regionEnd(self) -> int: ...
    def regionStart(self) -> int: ...
    def replaceAll(self, string: str) -> str: ...
    def replaceFirst(self, string: str) -> str: ...
    def requireEnd(self) -> bool: ...
    @overload
    def reset(self) -> 'Matcher': ...
    @overload
    def reset(self, charSequence: java.lang.CharSequence) -> 'Matcher': ...
    @overload
    def start(self) -> int: ...
    @overload
    def start(self, int: int) -> int: ...
    @overload
    def start(self, string: str) -> int: ...
    def toMatchResult(self) -> MatchResult: ...
    def toString(self) -> str: ...
    def useAnchoringBounds(self, boolean: bool) -> 'Matcher': ...
    def usePattern(self, pattern: Pattern) -> 'Matcher': ...
    def useTransparentBounds(self, boolean: bool) -> 'Matcher': ...
