from typing import List as _py_List
import java.util


class LocaleServiceProvider:
    def getAvailableLocales(self) -> _py_List[java.util.Locale]: ...
    def isSupportedLocale(self, locale: java.util.Locale) -> bool: ...

class ResourceBundleControlProvider:
    def getControl(self, string: str) -> java.util.ResourceBundle.Control: ...

class CalendarDataProvider(LocaleServiceProvider):
    def getFirstDayOfWeek(self, locale: java.util.Locale) -> int: ...
    def getMinimalDaysInFirstWeek(self, locale: java.util.Locale) -> int: ...

class CalendarNameProvider(LocaleServiceProvider):
    def getDisplayName(self, string: str, int: int, int2: int, int3: int, locale: java.util.Locale) -> str: ...
    def getDisplayNames(self, string: str, int: int, int2: int, locale: java.util.Locale) -> java.util.Map[str, int]: ...

class CurrencyNameProvider(LocaleServiceProvider):
    def getDisplayName(self, string: str, locale: java.util.Locale) -> str: ...
    def getSymbol(self, string: str, locale: java.util.Locale) -> str: ...

class LocaleNameProvider(LocaleServiceProvider):
    def getDisplayCountry(self, string: str, locale: java.util.Locale) -> str: ...
    def getDisplayLanguage(self, string: str, locale: java.util.Locale) -> str: ...
    def getDisplayScript(self, string: str, locale: java.util.Locale) -> str: ...
    def getDisplayVariant(self, string: str, locale: java.util.Locale) -> str: ...

class TimeZoneNameProvider(LocaleServiceProvider):
    def getDisplayName(self, string: str, boolean: bool, int: int, locale: java.util.Locale) -> str: ...
    def getGenericDisplayName(self, string: str, int: int, locale: java.util.Locale) -> str: ...
