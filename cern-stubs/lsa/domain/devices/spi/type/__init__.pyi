from typing import Any as _py_Any
from typing import List as _py_List
from typing import TypeVar as _py_TypeVar
from typing import Type as _py_Type
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.accsoft.commons.util
import cern.accsoft.commons.value
import cern.lsa.domain.commons.spi
import cern.lsa.domain.devices
import cern.lsa.domain.devices.type
import java.lang
import java.util


class PropertyBuilder:
    def __init__(self): ...
    def addField(self, propertyFieldInfo: 'PropertyBuilder.PropertyFieldInfo') -> 'PropertyBuilder': ...
    def build(self) -> cern.lsa.domain.devices.type.PropertyVersion: ...
    def hasFieldWithName(self, string: str) -> bool: ...
    def setCycleBound(self, boolean: bool) -> 'PropertyBuilder': ...
    def setDescription(self, string: str) -> 'PropertyBuilder': ...
    def setDeviceTypeVersion(self, deviceTypeVersion: cern.lsa.domain.devices.DeviceTypeVersion) -> 'PropertyBuilder': ...
    def setMonitorable(self, boolean: bool) -> 'PropertyBuilder': ...
    def setMultiplexed(self, boolean: bool) -> 'PropertyBuilder': ...
    def setName(self, string: str) -> 'PropertyBuilder': ...
    def setPropertyVisibility(self, propertyVisibility: cern.lsa.domain.devices.type.PropertyVersion.PropertyVisibility) -> 'PropertyBuilder': ...
    def setReadable(self, boolean: bool) -> 'PropertyBuilder': ...
    def setSupportingPartialSet(self, boolean: bool) -> 'PropertyBuilder': ...
    def setVirtual(self, boolean: bool) -> 'PropertyBuilder': ...
    def setWritable(self, boolean: bool) -> 'PropertyBuilder': ...
    class PropertyFieldInfo(cern.accsoft.commons.util.Named):
        def __init__(self, string: str, type: cern.accsoft.commons.value.Type, string2: str, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, string3: str, propertyField: cern.lsa.domain.devices.type.PropertyField): ...
        def getName(self) -> str: ...

class PropertyFieldAccess(java.lang.Enum['PropertyFieldAccess']):
    READ_ONLY: _py_ClassVar['PropertyFieldAccess'] = ...
    WRITE_ONLY: _py_ClassVar['PropertyFieldAccess'] = ...
    READ_WRITE: _py_ClassVar['PropertyFieldAccess'] = ...
    @classmethod
    def getFromString(cls, string: str) -> 'PropertyFieldAccess': ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'PropertyFieldAccess': ...
    _valueOf_1__T = _py_TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @classmethod
    @overload
    def valueOf(cls, class_: _py_Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @classmethod
    def values(cls) -> _py_List['PropertyFieldAccess']: ...

class PropertyFieldVersionImpl(cern.accsoft.commons.util.AbstractNamedSerializable[cern.lsa.domain.devices.type.PropertyFieldVersion], cern.lsa.domain.devices.type.PropertyFieldVersion):
    def equals(self, object: _py_Any) -> bool: ...
    def getDescription(self) -> str: ...
    def getPropertyField(self) -> cern.lsa.domain.devices.type.PropertyField: ...
    def getPropertyVersion(self) -> cern.lsa.domain.devices.type.PropertyVersion: ...
    def getValueDescriptor(self) -> cern.accsoft.commons.value.ValueDescriptor: ...
    def getValueType(self) -> cern.accsoft.commons.value.Type: ...
    def hashCode(self) -> int: ...
    def isReadable(self) -> bool: ...
    def isWritable(self) -> bool: ...
    def toString(self) -> str: ...

class PropertyVersionImpl(cern.accsoft.commons.util.AbstractNamedSerializable[cern.lsa.domain.devices.type.PropertyVersion], cern.lsa.domain.devices.type.PropertyVersion):
    def __init__(self, string: str, deviceTypeVersion: cern.lsa.domain.devices.DeviceTypeVersion, string2: str, boolean: bool, boolean2: bool, boolean3: bool, boolean4: bool, boolean5: bool, boolean6: bool, boolean7: bool, propertyVisibility: cern.lsa.domain.devices.type.PropertyVersion.PropertyVisibility): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getDescription(self) -> str: ...
    def getDeviceTypeVersion(self) -> cern.lsa.domain.devices.DeviceTypeVersion: ...
    def getFields(self) -> java.util.Set[cern.lsa.domain.devices.type.PropertyFieldVersion]: ...
    def getPropertyVisibility(self) -> cern.lsa.domain.devices.type.PropertyVersion.PropertyVisibility: ...
    def hashCode(self) -> int: ...
    def isCycleBound(self) -> bool: ...
    def isMonitorable(self) -> bool: ...
    def isMultiplexed(self) -> bool: ...
    def isReadable(self) -> bool: ...
    def isSupportingPartialSet(self) -> bool: ...
    def isVirtual(self) -> bool: ...
    def isWritable(self) -> bool: ...
    def toString(self) -> str: ...

class PropertyVersionsRequestImpl(cern.lsa.domain.commons.spi.AbstractPropertiesHolder, cern.lsa.domain.devices.type.PropertyVersionsRequest):
    PROPERTY_NAMES: _py_ClassVar[str] = ...
    DEVICE_TYPE_NAMES: _py_ClassVar[str] = ...
    DEVICE_TYPE_VERSIONS: _py_ClassVar[str] = ...
    def __init__(self, map: java.util.Map[str, _py_Any]): ...
    def getDeviceTypeNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeVersions(self) -> java.util.Set[cern.lsa.domain.devices.DeviceTypeVersion]: ...
    def getPropertyNames(self) -> java.util.Set[str]: ...
