from typing import Any as _py_Any
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.domain.zones
import cern.accsoft.commons.util
import cern.lsa.domain.commons
import cern.lsa.domain.commons.spi
import cern.lsa.domain.devices
import cern.lsa.domain.settings
import java.io
import java.util


class DeviceGroupImpl(cern.lsa.domain.commons.spi.AbstractIdentifiedNamedEntity[cern.lsa.domain.devices.DeviceGroup], cern.lsa.domain.devices.DeviceGroup):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, deviceGroup: cern.lsa.domain.devices.DeviceGroup): ...
    @overload
    def __init__(self, string: str, deviceGroupType: cern.lsa.domain.devices.DeviceGroupType, accelerator: cern.accsoft.commons.domain.Accelerator): ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getChildGroups(self) -> java.util.Set[cern.lsa.domain.devices.DeviceGroup]: ...
    def getCreateTime(self) -> java.util.Date: ...
    def getCreator(self) -> str: ...
    def getDisplayName(self) -> str: ...
    def getModifier(self) -> str: ...
    def getModifyTime(self) -> java.util.Date: ...
    def getType(self) -> cern.lsa.domain.devices.DeviceGroupType: ...
    def isOperational(self) -> bool: ...
    def setAccelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> None: ...
    def setChildGroups(self, set: java.util.Set[cern.lsa.domain.devices.DeviceGroup]) -> None: ...
    def setCreateTime(self, date: java.util.Date) -> None: ...
    def setCreator(self, string: str) -> None: ...
    def setDisplayName(self, string: str) -> None: ...
    def setModifier(self, string: str) -> None: ...
    def setModifyTime(self, date: java.util.Date) -> None: ...
    def setOperational(self, boolean: bool) -> None: ...
    def setType(self, deviceGroupType: cern.lsa.domain.devices.DeviceGroupType) -> None: ...
    def toString(self) -> str: ...

class DeviceGroupsRequestImpl(cern.lsa.domain.commons.spi.AbstractPropertiesHolder, cern.lsa.domain.devices.DeviceGroupsRequest):
    ACCELERATOR: _py_ClassVar[str] = ...
    DEVICE_GROUP_NAME_PATTERN: _py_ClassVar[str] = ...
    DEVICE_GROUP_NAMES: _py_ClassVar[str] = ...
    DEVICE_GROUP_TYPES: _py_ClassVar[str] = ...
    def __init__(self, map: java.util.Map[str, _py_Any]): ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getDeviceGroupNamePattern(self) -> str: ...
    def getDeviceGroupNames(self) -> java.util.Set[str]: ...
    def getDeviceGroupTypes(self) -> java.util.Set[cern.lsa.domain.devices.DeviceGroupType]: ...

class DeviceImpl(cern.lsa.domain.commons.spi.AbstractIdentifiedNamedEntity[cern.lsa.domain.devices.Device], cern.lsa.domain.devices.Device):
    @overload
    def __init__(self, device: cern.lsa.domain.devices.Device): ...
    @overload
    def __init__(self, long: int, string: str): ...
    def addDeviceGroup(self, string: str) -> None: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAcceleratorZone(self) -> cern.accsoft.commons.domain.zones.AcceleratorZone: ...
    def getAlias(self) -> str: ...
    def getDescription(self) -> str: ...
    def getDeviceGroups(self) -> java.util.Set[str]: ...
    def getDeviceType(self) -> cern.lsa.domain.devices.DeviceType: ...
    def getDeviceTypeVersion(self) -> cern.lsa.domain.devices.DeviceTypeVersion: ...
    def getFecName(self) -> str: ...
    def getPosition(self) -> float: ...
    def getServerName(self) -> str: ...
    def getSortOrder(self) -> int: ...
    def getState(self) -> cern.lsa.domain.devices.Device.DeviceState: ...
    def isCycleBound(self) -> bool: ...
    def isMultiplexed(self) -> bool: ...
    def setAcceleratorZone(self, acceleratorZone: cern.accsoft.commons.domain.zones.AcceleratorZone) -> None: ...
    def setAlias(self, string: str) -> None: ...
    def setCycleBound(self, boolean: bool) -> None: ...
    def setDescription(self, string: str) -> None: ...
    def setDeviceGroups(self, set: java.util.Set[str]) -> None: ...
    def setDeviceTypeVersion(self, deviceTypeVersion: cern.lsa.domain.devices.DeviceTypeVersion) -> None: ...
    def setFecName(self, string: str) -> None: ...
    def setMultiplexed(self, boolean: bool) -> None: ...
    def setPosition(self, double: float) -> None: ...
    def setServerName(self, string: str) -> None: ...
    def setSortOrder(self, int: int) -> None: ...
    def setState(self, deviceState: cern.lsa.domain.devices.Device.DeviceState) -> None: ...
    def toString(self) -> str: ...

class DeviceTypeImpl(cern.lsa.domain.commons.spi.AbstractIdentifiedNamedEntity[cern.lsa.domain.devices.DeviceType], cern.lsa.domain.devices.DeviceType):
    def __init__(self, long: int, string: str): ...
    def addVersion(self, deviceTypeVersion: cern.lsa.domain.devices.DeviceTypeVersion) -> None: ...
    @overload
    def compareTo(self, named: cern.accsoft.commons.util.Named) -> int: ...
    @overload
    def compareTo(self, deviceType: cern.lsa.domain.devices.DeviceType) -> int: ...
    @overload
    def compareTo(self, object: _py_Any) -> int: ...
    def getDescription(self) -> str: ...
    def getMetaType(self) -> cern.lsa.domain.devices.DeviceMetaTypeEnum: ...
    def getVersions(self) -> java.util.SortedSet[cern.lsa.domain.devices.DeviceTypeVersion]: ...
    def setDescription(self, string: str) -> None: ...
    def setMetaType(self, deviceMetaTypeEnum: cern.lsa.domain.devices.DeviceMetaTypeEnum) -> None: ...
    def setVersions(self, set: java.util.Set[cern.lsa.domain.devices.DeviceTypeVersion]) -> None: ...
    def toString(self) -> str: ...

class DeviceTypeVersionImpl(cern.lsa.domain.commons.spi.AbstractIdentifiedEntity[cern.lsa.domain.devices.DeviceTypeVersion], cern.lsa.domain.devices.DeviceTypeVersion):
    def __init__(self, long: int, deviceType: cern.lsa.domain.devices.DeviceType, deviceTypeImplementation: cern.lsa.domain.devices.DeviceTypeImplementation, deviceTypeVersionNumber: cern.lsa.domain.devices.DeviceTypeVersionNumber): ...
    @overload
    def compareTo(self, identifiedEntity: cern.lsa.domain.commons.IdentifiedEntity) -> int: ...
    @overload
    def compareTo(self, deviceTypeVersion: cern.lsa.domain.devices.DeviceTypeVersion) -> int: ...
    @overload
    def compareTo(self, object: _py_Any) -> int: ...
    def equals(self, object: _py_Any) -> bool: ...
    def getDeviceType(self) -> cern.lsa.domain.devices.DeviceType: ...
    def getImplementation(self) -> cern.lsa.domain.devices.DeviceTypeImplementation: ...
    def getVersionNumber(self) -> cern.lsa.domain.devices.DeviceTypeVersionNumber: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...

class DeviceTypeVersionNumberImpl(cern.lsa.domain.devices.DeviceTypeVersionNumber, java.io.Serializable):
    def __init__(self, int: int, int2: int): ...
    @overload
    def compareTo(self, deviceTypeVersionNumber: cern.lsa.domain.devices.DeviceTypeVersionNumber) -> int: ...
    @overload
    def compareTo(self, object: _py_Any) -> int: ...
    def equals(self, object: _py_Any) -> bool: ...
    def getMajor(self) -> int: ...
    def getMinor(self) -> int: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...

class DeviceTypesRequestImpl(cern.lsa.domain.commons.spi.AbstractPropertiesHolder, cern.lsa.domain.devices.DeviceTypesRequest):
    ACCELERATOR: _py_ClassVar[str] = ...
    ALL_DEV_TYPES_REQUESTED: _py_ClassVar[str] = ...
    DEVICE_TYPE_NAMES: _py_ClassVar[str] = ...
    DEVICE_TYPE_NAME_PATTERN: _py_ClassVar[str] = ...
    DEVICE_TYPE_VERSION_NUMBER: _py_ClassVar[str] = ...
    DEVICE_TYPE_IMPLEMENTATION: _py_ClassVar[str] = ...
    def __init__(self, map: java.util.Map[str, _py_Any]): ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAllTypesRequested(self) -> bool: ...
    def getDeviceTypeImplementations(self) -> java.util.Set[cern.lsa.domain.devices.DeviceTypeImplementation]: ...
    def getDeviceTypeNamePattern(self) -> str: ...
    def getDeviceTypeNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeVersionNumber(self) -> cern.lsa.domain.devices.DeviceTypeVersionNumber: ...

class DevicesRequestImpl(cern.lsa.domain.commons.spi.AbstractPropertiesHolder, cern.lsa.domain.devices.DevicesRequest):
    ACCELERATOR: _py_ClassVar[str] = ...
    ACCELERATOR_ZONES: _py_ClassVar[str] = ...
    PARTICLE_TRANSFERS: _py_ClassVar[str] = ...
    DEVICE_TYPE_NAMES: _py_ClassVar[str] = ...
    DEVICE_TYPE_NAME_PATTERN: _py_ClassVar[str] = ...
    META_TYPE: _py_ClassVar[str] = ...
    DEVICE_NAMES: _py_ClassVar[str] = ...
    DEVICE_NAME_PATTERN: _py_ClassVar[str] = ...
    MULTIPLEXED: _py_ClassVar[str] = ...
    DEVICE_GROUP_IDS: _py_ClassVar[str] = ...
    DEVICE_GROUP_NAMES: _py_ClassVar[str] = ...
    ELEMENT_NAME: _py_ClassVar[str] = ...
    DEVICE_ALIAS: _py_ClassVar[str] = ...
    FEC_NAMES: _py_ClassVar[str] = ...
    DEVICE_TYPE_VERSIONS: _py_ClassVar[str] = ...
    EXIST_IN_LSA_ONLY: _py_ClassVar[str] = ...
    def __init__(self, map: java.util.Map[str, _py_Any]): ...
    def existInLsaOnly(self) -> bool: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAcceleratorZones(self) -> java.util.Set[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDeviceAliases(self) -> java.util.Set[str]: ...
    def getDeviceGroupIds(self) -> java.util.Set[int]: ...
    def getDeviceGroupNames(self) -> java.util.Set[str]: ...
    def getDeviceNamePattern(self) -> str: ...
    def getDeviceNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeNamePattern(self) -> str: ...
    def getDeviceTypeNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeVersions(self) -> java.util.Set[cern.lsa.domain.devices.DeviceTypeVersion]: ...
    def getElementName(self) -> str: ...
    def getFecNames(self) -> java.util.Set[str]: ...
    def getMetaType(self) -> cern.lsa.domain.devices.DeviceMetaTypeEnum: ...
    def getParticleTransfers(self) -> java.util.Set[cern.accsoft.commons.domain.particletransfers.ParticleTransfer]: ...
    def isMultiplexed(self) -> bool: ...

class ParameterGroupImpl(cern.lsa.domain.commons.spi.AbstractIdentifiedNamedEntity[cern.lsa.domain.settings.ParameterGroup], cern.lsa.domain.settings.ParameterGroup):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, long: int, string: str): ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getCreateDate(self) -> java.util.Date: ...
    def getCreator(self) -> str: ...
    def getDescription(self) -> str: ...
    def setAccelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> None: ...
    def setCreateDate(self, date: java.util.Date) -> None: ...
    def setCreator(self, string: str) -> None: ...
    def setDescription(self, string: str) -> None: ...
    def toString(self) -> str: ...
