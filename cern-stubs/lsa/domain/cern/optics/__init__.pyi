from typing import List as _py_List
from typing import TypeVar as _py_TypeVar
from typing import Type as _py_Type
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.accsoft.commons.util
import cern.accsoft.commons.value
import cern.lsa.domain.commons
import java.lang
import java.util


class FidelModel(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity):
    def findModelCoefficients(self, string: str) -> java.util.Map[str, float]: ...
    def getComponentsNames(self) -> java.util.Set[str]: ...
    def getModelId(self) -> int: ...
    def getModelName(self) -> str: ...

class FidelTaskResult:
    def getAllMessages(self) -> str: ...
    def getExceptions(self) -> java.util.List[java.lang.Exception]: ...
    def getLastException(self) -> java.lang.Exception: ...
    def getMessages(self) -> java.util.List[str]: ...
    def getResult(self) -> 'FidelTaskResult.FidelTaskResultEnum': ...
    def merge(self, fidelTaskResult: 'FidelTaskResult') -> 'FidelTaskResult': ...
    class FidelTaskResultEnum(java.lang.Enum['FidelTaskResult.FidelTaskResultEnum']):
        ERROR: _py_ClassVar['FidelTaskResult.FidelTaskResultEnum'] = ...
        WARNING: _py_ClassVar['FidelTaskResult.FidelTaskResultEnum'] = ...
        SUCCESS: _py_ClassVar['FidelTaskResult.FidelTaskResultEnum'] = ...
        @classmethod
        @overload
        def valueOf(cls, string: str) -> 'FidelTaskResult.FidelTaskResultEnum': ...
        _valueOf_1__T = _py_TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
        @classmethod
        @overload
        def valueOf(cls, class_: _py_Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
        @classmethod
        def values(cls) -> _py_List['FidelTaskResult.FidelTaskResultEnum']: ...

class FieldHarmonic(cern.accsoft.commons.util.Named):
    def getComponentName(self) -> str: ...
    def getCreationDate(self) -> java.util.Date: ...
    def getCurrent2CnFunction(self) -> cern.accsoft.commons.value.ImmutableDiscreteFunction: ...
    def getModelName(self) -> str: ...
    def getName(self) -> str: ...

class MasterControllerTimingEvent(cern.accsoft.commons.util.Named):
    def getTimingParameterName(self) -> str: ...
