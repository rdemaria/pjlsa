from typing import List as _py_List
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.accsoft.commons.domain
import cern.japc.core
import cern.japc.value
import cern.lsa.domain
import cern.lsa.domain.commons
import cern.lsa.domain.devices
import cern.lsa.domain.devices.type
import cern.lsa.domain.exploitation
import cern.lsa.domain.exploitation.command
import cern.lsa.domain.optics
import cern.lsa.domain.settings
import cern.lsa.domain.settings.parameter.relation
import cern.lsa.domain.settings.parameter.type.relation
import cern.lsa.domain.settings.type
import cern.lsa.domain.trim.rules.makerule
import java.lang
import java.util


class ClientException(cern.lsa.domain.LsaException):
    @overload
    def __init__(self, exception: java.lang.Exception): ...
    @overload
    def __init__(self, string: str): ...
    @overload
    def __init__(self, string: str, exception: java.lang.Exception): ...

class CommonAcceleratorService:
    def findAccelerator(self, string: str) -> cern.accsoft.commons.domain.Accelerator: ...
    def findAccelerators(self) -> java.util.Set[cern.accsoft.commons.domain.Accelerator]: ...

class CommonArchiveReferenceService:
    def createArchive(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext, string: str, string2: str) -> cern.lsa.domain.settings.Archive: ...
    def createArchiveVersion(self, archive: cern.lsa.domain.settings.Archive, string: str, stringArray: _py_List[str]) -> cern.lsa.domain.settings.Archive: ...
    def deleteArchive(self, archive: cern.lsa.domain.settings.Archive) -> None: ...
    def deleteArchiveVersion(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion) -> cern.lsa.domain.settings.Archive: ...
    def findArchiveById(self, long: int) -> cern.lsa.domain.settings.Archive: ...
    @overload
    def findArchiveSettings(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion) -> cern.lsa.domain.settings.CompositeContextSettings: ...
    @overload
    def findArchiveSettings(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, collection: java.util.Collection[str]) -> cern.lsa.domain.settings.CompositeContextSettings: ...
    def findArchives(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.List[cern.lsa.domain.settings.Archive]: ...
    def findParametersWithoutSettings(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, stringArray: _py_List[str]) -> java.util.Set[str]: ...
    @overload
    def findReferenceValues(self, drivableContext: cern.lsa.domain.settings.DrivableContext) -> cern.lsa.domain.settings.CompositeContextSettings: ...
    @overload
    def findReferenceValues(self, drivableContext: cern.lsa.domain.settings.DrivableContext, collection: java.util.Collection[str]) -> cern.lsa.domain.settings.CompositeContextSettings: ...
    def restoreArchive(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, stringArray: _py_List[str]) -> cern.lsa.domain.settings.SettingsRestoreStatus: ...
    def restoreReferences(self, drivableContext: cern.lsa.domain.settings.DrivableContext, collection: java.util.Collection[str]) -> cern.lsa.domain.settings.SettingsRestoreStatus: ...
    def saveReferenceValues(self, drivableContext: cern.lsa.domain.settings.DrivableContext, collection: java.util.Collection[str]) -> java.util.Set[str]: ...
    def updateArchiveName(self, archive: cern.lsa.domain.settings.Archive, string: str) -> cern.lsa.domain.settings.Archive: ...
    def updateArchiveVersionDescription(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, string: str) -> cern.lsa.domain.settings.Archive: ...

class CommonCacheService:
    def clearAll(self) -> None: ...

class CommonContextService:
    def findAcceleratorUser(self, acceleratorUsersRequest: cern.lsa.domain.settings.AcceleratorUsersRequest) -> cern.lsa.domain.settings.AcceleratorUser: ...
    def findAcceleratorUsers(self, acceleratorUsersRequest: cern.lsa.domain.settings.AcceleratorUsersRequest) -> java.util.Set[cern.lsa.domain.settings.AcceleratorUser]: ...
    def findBeamProcessPurposes(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.BeamProcessPurpose]: ...
    def findContextCategories(self) -> java.util.Set[cern.lsa.domain.settings.ContextCategory]: ...
    def findDefaultBeamProcessPurpose(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> cern.lsa.domain.settings.type.BeamProcessPurpose: ...
    def findDefaultContextCategory(self) -> cern.lsa.domain.settings.ContextCategory: ...
    def findDrivableContextByAcceleratorUser(self, acceleratorUser: cern.lsa.domain.settings.AcceleratorUser) -> cern.lsa.domain.settings.DrivableContext: ...
    def findDrivableContextByUser(self, string: str) -> cern.lsa.domain.settings.DrivableContext: ...
    def findResidentContexts(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]: ...
    def findResidentNonMultiplexedContext(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> cern.lsa.domain.settings.StandAloneContext: ...
    def findStandAloneBeamProcess(self, string: str) -> cern.lsa.domain.settings.StandAloneBeamProcess: ...
    @overload
    def findStandAloneBeamProcesses(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneBeamProcess]: ...
    @overload
    def findStandAloneBeamProcesses(self, standAloneBeamProcessesRequest: cern.lsa.domain.settings.StandAloneBeamProcessesRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneBeamProcess]: ...
    def findStandAloneContextByAcceleratorUser(self, acceleratorUser: cern.lsa.domain.settings.AcceleratorUser) -> cern.lsa.domain.settings.StandAloneContext: ...
    def findStandAloneContextByUser(self, string: str) -> cern.lsa.domain.settings.StandAloneContext: ...
    def findStandAloneContexts(self, standAloneContextsRequest: cern.lsa.domain.settings.StandAloneContextsRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]: ...
    def findStandAloneCycle(self, string: str) -> cern.lsa.domain.settings.StandAloneCycle: ...
    @overload
    def findStandAloneCycles(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]: ...
    @overload
    def findStandAloneCycles(self, standAloneCyclesRequest: cern.lsa.domain.settings.StandAloneCyclesRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]: ...
    def findUserContextMappingHistory(self, accelerator: cern.accsoft.commons.domain.Accelerator, contextFamily: cern.lsa.domain.settings.ContextFamily, long: int, long2: int) -> java.util.List[cern.lsa.domain.settings.UserContextMapping]: ...
    def findUsers(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[str]: ...
    def saveContextToUserMapping(self, collection: java.util.Collection[cern.lsa.domain.settings.StandAloneContext]) -> None: ...
    def updateContext(self, context: cern.lsa.domain.settings.Context) -> None: ...

class CommonDeviceService:
    def deleteDeviceGroups(self, collection: java.util.Collection[cern.lsa.domain.devices.DeviceGroup]) -> None: ...
    def findActualDevicesByLogicalHardwareName(self, collection: java.util.Collection[str]) -> java.util.Map[str, java.util.Set[cern.lsa.domain.devices.Device]]: ...
    def findCalibration(self, calibrationsRequest: cern.lsa.domain.devices.CalibrationsRequest) -> cern.lsa.domain.optics.Calibration: ...
    def findCalibrations(self, calibrationsRequest: cern.lsa.domain.devices.CalibrationsRequest) -> java.util.Set[cern.lsa.domain.optics.Calibration]: ...
    def findDevice(self, string: str) -> cern.lsa.domain.devices.Device: ...
    def findDeviceGroupTypes(self) -> java.util.Set[cern.lsa.domain.devices.DeviceGroupType]: ...
    def findDeviceGroups(self, deviceGroupsRequest: cern.lsa.domain.devices.DeviceGroupsRequest) -> java.util.Set[cern.lsa.domain.devices.DeviceGroup]: ...
    def findDeviceType(self, string: str) -> cern.lsa.domain.devices.DeviceType: ...
    def findDeviceTypes(self, deviceTypesRequest: cern.lsa.domain.devices.DeviceTypesRequest) -> java.util.Set[cern.lsa.domain.devices.DeviceType]: ...
    def findDevices(self, devicesRequest: cern.lsa.domain.devices.DevicesRequest) -> java.util.Set[cern.lsa.domain.devices.Device]: ...
    def findDevicesByGroups(self, collection: java.util.Collection[cern.lsa.domain.devices.DeviceGroup]) -> java.util.Map[cern.lsa.domain.devices.DeviceGroup, java.util.List[cern.lsa.domain.devices.Device]]: ...
    def findLogicalHardware(self, devicesRequest: cern.lsa.domain.devices.DevicesRequest) -> java.util.Set[cern.lsa.domain.optics.LogicalHardware]: ...
    def findLogicalHardwaresByActualDeviceNames(self, collection: java.util.Collection[str]) -> java.util.Map[str, java.util.Set[cern.lsa.domain.optics.LogicalHardware]]: ...
    def findLogicalNamesByMadStrengthNames(self, collection: java.util.Collection[str]) -> java.util.Map[str, str]: ...
    def findMadStrengthNamesByLogicalNames(self, collection: java.util.Collection[str]) -> java.util.Map[str, str]: ...
    def findPowerConverterInfo(self, string: str) -> cern.lsa.domain.optics.PowerConverterInfo: ...
    def findPropertyFields(self, propertyFieldsRequest: cern.lsa.domain.devices.type.PropertyFieldsRequest) -> java.util.Set[cern.lsa.domain.devices.type.PropertyField]: ...
    def findPropertyVersions(self, propertyVersionsRequest: cern.lsa.domain.devices.type.PropertyVersionsRequest) -> java.util.SortedMap[cern.lsa.domain.devices.DeviceTypeVersion, java.util.Set[cern.lsa.domain.devices.type.PropertyVersion]]: ...
    def saveDeviceGroup(self, deviceGroup: cern.lsa.domain.devices.DeviceGroup) -> None: ...
    def saveDeviceGroupDevices(self, deviceGroup: cern.lsa.domain.devices.DeviceGroup, collection: java.util.Collection[cern.lsa.domain.devices.Device]) -> None: ...
    def saveLogicalHardware(self, logicalHardware: cern.lsa.domain.optics.LogicalHardware) -> None: ...
    def setActiveCalibration(self, string: str, string2: str) -> None: ...

class CommonExploitationService:
    def drive(self, driveRequest: cern.lsa.domain.exploitation.DriveRequest) -> cern.lsa.domain.exploitation.DriveResult: ...
    def executeHwCommand(self, hwCommandExecutionRequest: cern.lsa.domain.exploitation.command.HwCommandExecutionRequest) -> cern.lsa.domain.exploitation.command.HwCommandExecutionResponse: ...
    def findHwCommands(self, string: str) -> cern.lsa.domain.exploitation.command.HwCommandSet: ...
    def performSettingsCheck(self, settingsOnlineCheckRequest: cern.lsa.domain.exploitation.SettingsOnlineCheckRequest) -> java.util.List[cern.japc.core.FailSafeParameterValue]: ...
    def readHardwareValues(self, readHardwareRequest: cern.lsa.domain.exploitation.ReadHardwareRequest) -> java.util.Map[cern.lsa.domain.settings.Parameter, cern.lsa.domain.exploitation.FailSafeImmutableValue]: ...

class CommonGenerationService:
    def cloneStandAloneContext(self, standAloneContextCloneRequest: cern.lsa.domain.settings.StandAloneContextCloneRequest) -> cern.lsa.domain.settings.StandAloneContext: ...
    def createStandAloneContext(self, standAloneContextCreationRequest: cern.lsa.domain.settings.StandAloneContextCreationRequest) -> cern.lsa.domain.settings.StandAloneContext: ...
    def deleteContextType(self, contextType: cern.lsa.domain.settings.type.ContextType) -> None: ...
    def deleteStandAloneContext(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> None: ...
    def findAllParticleTypes(self) -> java.util.Set[cern.accsoft.commons.domain.ParticleType]: ...
    def findBeamProcessType(self, string: str) -> cern.lsa.domain.settings.type.BeamProcessType: ...
    def findBeamProcessTypeAttributeDefinitions(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.BeamProcessTypeSegmentAttributeDefinition]: ...
    def findBeamProcessTypeOpticTable(self, string: str) -> cern.lsa.domain.optics.OpticsTable: ...
    def findBeamProcessTypes(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.BeamProcessType]: ...
    def findContextNamesByType(self, contextType: cern.lsa.domain.settings.type.ContextType) -> java.util.Set[str]: ...
    def findCycleType(self, string: str) -> cern.lsa.domain.settings.type.CycleType: ...
    def findCycleTypeAttributeDefinitions(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.commons.AttributeDefinition]: ...
    def findCycleTypes(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.CycleType]: ...
    def findIncorporationRanges(self, string: str) -> java.util.List[cern.lsa.domain.settings.type.IncorporationRange]: ...
    def findIncorporationRules(self) -> java.util.List[cern.lsa.domain.settings.type.IncorporationRuleDescriptor]: ...
    def findStandAloneContexAttributeDefinitions(self, contextFamily: cern.lsa.domain.settings.ContextFamily, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.commons.AttributeDefinition]: ...
    def generateActualBeamProcess(self, beamProcess: cern.lsa.domain.settings.BeamProcess, int: int) -> cern.lsa.domain.settings.StandAloneBeamProcess: ...
    def generateSettings(self, settingsGenerationRequest: cern.lsa.domain.settings.SettingsGenerationRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    def regenerateActualBeamProcess(self, standAloneBeamProcess: cern.lsa.domain.settings.StandAloneBeamProcess, string: str) -> None: ...
    def saveBeamProcessType(self, beamProcessType: cern.lsa.domain.settings.type.BeamProcessType) -> None: ...
    def saveCycleType(self, cycleType: cern.lsa.domain.settings.type.CycleType) -> None: ...
    def saveIncorporationRanges(self, collection: java.util.Collection[cern.lsa.domain.settings.type.IncorporationRange], string: str) -> None: ...
    def saveOpticTable(self, opticsTable: cern.lsa.domain.optics.OpticsTable) -> None: ...

class CommonJapcService:
    def getValue(self, string: str, selector: cern.japc.core.Selector) -> cern.japc.core.AcquiredParameterValue: ...
    def getValues(self, collection: java.util.Collection[str], selector: cern.japc.core.Selector) -> java.util.List[cern.japc.core.FailSafeParameterValue]: ...
    def setValue(self, string: str, selector: cern.japc.core.Selector, parameterValue: cern.japc.value.ParameterValue) -> None: ...

class CommonKnobService:
    def deactivateKnob(self, string: str) -> None: ...
    def deleteKnob(self, string: str) -> None: ...
    def findKnob(self, string: str) -> cern.lsa.domain.settings.Knob: ...
    def findKnobs(self, parametersRequest: cern.lsa.domain.settings.ParametersRequest) -> java.util.Set[cern.lsa.domain.settings.Knob]: ...
    def saveKnob(self, knob: cern.lsa.domain.settings.Knob) -> cern.lsa.domain.settings.Knob: ...

class CommonOpticService:
    def deleteMeasuredTwiss(self, measuredTwissArray: _py_List[cern.lsa.domain.optics.MeasuredTwiss]) -> int: ...
    def findElement(self, string: str) -> cern.lsa.domain.optics.Element: ...
    def findElements(self, elementsRequest: cern.lsa.domain.optics.ElementsRequest) -> java.util.Set[cern.lsa.domain.optics.Element]: ...
    @overload
    def findMeasuredTwiss(self, twiss: cern.lsa.domain.optics.Twiss) -> java.util.List[cern.lsa.domain.optics.MeasuredTwiss]: ...
    @overload
    def findMeasuredTwiss(self, string: str) -> java.util.List[cern.lsa.domain.optics.MeasuredTwiss]: ...
    def findOpticById(self, long: int) -> cern.lsa.domain.optics.Optic: ...
    def findOpticByName(self, string: str) -> cern.lsa.domain.optics.Optic: ...
    def findOpticInBeamProcessType(self, string: str, int: int) -> cern.lsa.domain.optics.Optic: ...
    def findOpticNames(self, opticsRequest: cern.lsa.domain.optics.OpticsRequest) -> java.util.Set[str]: ...
    def findOptics(self, collection: java.util.Collection[str]) -> java.util.Set[cern.lsa.domain.optics.Optic]: ...
    def findTwisses(self, twissesRequest: cern.lsa.domain.optics.TwissesRequest) -> java.util.Set[cern.lsa.domain.optics.Twiss]: ...
    def insertMeasuredTwiss(self, measuredTwissArray: _py_List[cern.lsa.domain.optics.MeasuredTwiss]) -> None: ...
    def renameOptic(self, string: str, string2: str) -> None: ...
    def saveElements(self, collection: java.util.Collection[cern.lsa.domain.optics.Element]) -> None: ...
    def setElementsObsolete(self, collection: java.util.Collection[cern.lsa.domain.optics.Element]) -> None: ...
    def updateElementName(self, string: str, string2: str) -> None: ...
    def updateElements(self, collection: java.util.Collection[cern.lsa.domain.optics.Element]) -> None: ...

class CommonParameterService:
    def addParametersToParameterGroup(self, parameterGroup: cern.lsa.domain.settings.ParameterGroup, collection: java.util.Collection[str]) -> None: ...
    def deleteCriticalProperty(self, propertyVersion: cern.lsa.domain.devices.type.PropertyVersion, device: cern.lsa.domain.devices.Device) -> None: ...
    def deleteParameterGroup(self, long: int) -> None: ...
    def deleteParameterRelations(self, collection: java.util.Collection[cern.lsa.domain.settings.parameter.relation.ParameterRelation]) -> None: ...
    def deleteParameterTypeRelations(self, collection: java.util.Collection[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelation]) -> None: ...
    def deleteParameterTypes(self, collection: java.util.Collection[str]) -> None: ...
    def deleteParameters(self, collection: java.util.Collection[str]) -> None: ...
    def findAllAvailableMakerules(self) -> java.util.Set[cern.lsa.domain.trim.rules.makerule.MakeRuleInfo]: ...
    def findAllHierarchies(self) -> java.util.List[str]: ...
    def findAllParameterTypes(self) -> java.util.Set[cern.lsa.domain.settings.ParameterType]: ...
    def findCommonHierarchyNames(self, collection: java.util.Collection[cern.lsa.domain.settings.Parameter]) -> java.util.Set[str]: ...
    def findHierarchyNames(self, collection: java.util.Collection[cern.lsa.domain.settings.Parameter]) -> java.util.Set[str]: ...
    def findMakeRuleForParameterRelation(self, parameterRelation: cern.lsa.domain.settings.parameter.relation.ParameterRelation) -> cern.lsa.domain.trim.rules.makerule.MakeRuleConfigInfo: ...
    def findParameterByName(self, string: str) -> cern.lsa.domain.settings.Parameter: ...
    def findParameterGroupsByAccelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.ParameterGroup]: ...
    def findParameterRelationInfos(self, parameterRelationInfosRequest: cern.lsa.domain.settings.parameter.relation.ParameterRelationInfosRequest) -> java.util.Set[cern.lsa.domain.settings.parameter.relation.ParameterRelationInfo]: ...
    def findParameterTrees(self, parameterTreesRequest: cern.lsa.domain.settings.ParameterTreesRequest) -> java.util.Set[cern.lsa.domain.settings.ParameterTreeNode]: ...
    def findParameterTypeRelationInfos(self, parameterTypeRelationInfosRequest: cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfosRequest) -> java.util.Set[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfo]: ...
    def findParameterTypes(self, parameterTypesRequest: cern.lsa.domain.settings.ParameterTypesRequest) -> java.util.Set[cern.lsa.domain.settings.ParameterType]: ...
    def findParameters(self, parametersRequest: cern.lsa.domain.settings.ParametersRequest) -> java.util.Set[cern.lsa.domain.settings.Parameter]: ...
    def findParametersByDeviceProperty(self, string: str) -> java.util.Set[cern.lsa.domain.settings.Parameter]: ...
    def findParametersForEditing(self, parametersRequest: cern.lsa.domain.settings.ParametersRequest) -> java.util.Set[cern.lsa.domain.settings.ParameterForEditing]: ...
    def findParametersWithSettings(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.Set[cern.lsa.domain.settings.Parameter]: ...
    def findParametersWithoutSettings(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.Set[cern.lsa.domain.settings.Parameter]: ...
    def getMaxDelta(self, parameter: cern.lsa.domain.settings.Parameter) -> float: ...
    def removeParametersFromParameterGroup(self, parameterGroup: cern.lsa.domain.settings.ParameterGroup, collection: java.util.Collection[str]) -> None: ...
    def saveCriticalProperty(self, propertyVersion: cern.lsa.domain.devices.type.PropertyVersion, device: cern.lsa.domain.devices.Device) -> None: ...
    def saveParameterGroup(self, parameterGroup: cern.lsa.domain.settings.ParameterGroup) -> None: ...
    def saveParameterRelationInfos(self, collection: java.util.Collection[cern.lsa.domain.settings.parameter.relation.ParameterRelationInfo]) -> None: ...
    def saveParameterTypeRelationInfos(self, collection: java.util.Collection[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfo]) -> None: ...
    def saveParameterTypes(self, collection: java.util.Collection[cern.lsa.domain.settings.ParameterType]) -> None: ...
    def saveParameters(self, collection: java.util.Collection[cern.lsa.domain.settings.ParameterAttributes]) -> None: ...

class CommonServiceLocator:
    JDBC_PROPERTIES: _py_ClassVar[str] = ...
    DATABASE_PROPERTY_NAME: _py_ClassVar[str] = ...
    SERVER_PROPERTIES: _py_ClassVar[str] = ...
    SERVER_PROPERTY_NAME: _py_ClassVar[str] = ...
    MODE_PROPERTY_NAME: _py_ClassVar[str] = ...
    @classmethod
    def isTwoTier(cls) -> bool: ...

class CommonSettingService:
    def findContextSettings(self, contextSettingsRequest: cern.lsa.domain.settings.ContextSettingsRequest) -> cern.lsa.domain.settings.ContextSettings: ...
    def findNotIncorporatedParameters(self, standAloneBeamProcess: cern.lsa.domain.settings.StandAloneBeamProcess, int: int, beamProcess: cern.lsa.domain.settings.BeamProcess, int2: int) -> cern.lsa.domain.settings.NotIncorporatedParameters: ...

class CommonTimingService: ...

class CommonTrimService:
    def copySettings(self, copySettingsRequest: cern.lsa.domain.settings.CopySettingsRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    def deleteTrimById(self, long: int) -> None: ...
    def findParametersAndParameterGroupsByBPsSinceTime(self, collection: java.util.Collection[cern.lsa.domain.settings.BeamProcess], date: java.util.Date) -> java.util.Map[str, java.util.Set[str]]: ...
    def findTrimHeaders(self, trimHeadersRequest: cern.lsa.domain.settings.TrimHeadersRequest) -> java.util.List[cern.lsa.domain.settings.TrimHeader]: ...
    def findTrimmedParameters(self, long: int) -> java.util.Set[cern.lsa.domain.settings.Parameter]: ...
    @overload
    def incorporate(self, beamProcessIncorporationRequest: cern.lsa.domain.settings.BeamProcessIncorporationRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @overload
    def incorporate(self, incorporationRequest: cern.lsa.domain.settings.IncorporationRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @overload
    def revertTrim(self, revertTrimRequest: cern.lsa.domain.settings.RevertTrimRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @overload
    def revertTrim(self, list: java.util.List[cern.lsa.domain.settings.RevertTrimRequest]) -> java.util.List[cern.lsa.domain.settings.TrimResponse]: ...
    @overload
    def trimSettings(self, trimRequest: cern.lsa.domain.settings.TrimRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @overload
    def trimSettings(self, collection: java.util.Collection[cern.lsa.domain.settings.TrimRequest]) -> java.util.List[cern.lsa.domain.settings.TrimResponse]: ...
    def updateTrimDescription(self, long: int, string: str) -> None: ...

class LsaConfigurationConstants:
    SYSPROP_DATABASE_PROPERTIES_FILENAME: _py_ClassVar[str] = ...
    SYSPROP_DATABASE_NAME: _py_ClassVar[str] = ...
    SYSPROP_SERVER_PROPERTIES_FILENAME: _py_ClassVar[str] = ...
    SYSPROP_SERVER_NAME: _py_ClassVar[str] = ...
    SYSPROP_MODE_NAME: _py_ClassVar[str] = ...
    MODE_3_TIER: _py_ClassVar[str] = ...
    MODE_2_TIER: _py_ClassVar[str] = ...
    def __init__(self): ...

class TransactionService:
    def generateTransactionId(self) -> int: ...
