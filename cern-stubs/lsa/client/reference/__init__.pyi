from typing import overload
import cern.accsoft.commons.util.value
import cern.japc.core
import cern.japc.value
import java.lang
import java.util


class ParameterReferences:
    def __init__(self, string: str, map: java.util.Map[cern.japc.core.Selector, cern.accsoft.commons.util.value.FailSafeValue[cern.japc.value.SimpleParameterValue]]): ...
    def getParameterName(self) -> str: ...
    def getReference(self, selector: cern.japc.core.Selector) -> cern.accsoft.commons.util.value.FailSafeValue[cern.japc.value.SimpleParameterValue]: ...
    def getReferences(self) -> java.util.Map[cern.japc.core.Selector, cern.accsoft.commons.util.value.FailSafeValue[cern.japc.value.SimpleParameterValue]]: ...
    def getSelectors(self) -> java.util.Set[cern.japc.core.Selector]: ...

class ReferenceController:
    def addReferenceListener(self, string: str, selector: cern.japc.core.Selector, referenceListener: 'ReferenceListener') -> None: ...
    def getReference(self, string: str, selector: cern.japc.core.Selector) -> cern.japc.value.SimpleParameterValue: ...
    @overload
    def getReferences(self, string: str, collection: java.util.Collection[cern.japc.core.Selector]) -> ParameterReferences: ...
    @overload
    def getReferences(self, collection: java.util.Collection[str], selector: cern.japc.core.Selector) -> java.util.Map[str, cern.accsoft.commons.util.value.FailSafeValue[cern.japc.value.SimpleParameterValue]]: ...
    def removeReferenceListener(self, string: str, selector: cern.japc.core.Selector) -> None: ...

class ReferenceException(java.lang.RuntimeException):
    @overload
    def __init__(self, string: str): ...
    @overload
    def __init__(self, string: str, throwable: java.lang.Throwable): ...
    @overload
    def __init__(self, throwable: java.lang.Throwable): ...

class ReferenceListener:
    def onReferenceChanged(self, string: str, selector: cern.japc.core.Selector, failSafeValue: cern.accsoft.commons.util.value.FailSafeValue[cern.japc.value.SimpleParameterValue]) -> None: ...
