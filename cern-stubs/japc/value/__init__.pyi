from typing import Any as _py_Any
from typing import List as _py_List
from typing import TypeVar as _py_TypeVar
from typing import Type as _py_Type
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.accsoft.commons.util
import java.io
import java.lang
import java.util


class Array2D:
    def getArray1D(self) -> _py_Any: ...
    def getBoolean(self, int: int, int2: int) -> bool: ...
    def getBooleanArray2D(self) -> _py_List[_py_List[bool]]: ...
    def getBooleanRow(self, int: int) -> _py_List[bool]: ...
    def getBooleans(self) -> _py_List[bool]: ...
    def getByte(self, int: int, int2: int) -> int: ...
    def getByteArray2D(self) -> _py_List[_py_List[int]]: ...
    def getByteRow(self, int: int) -> _py_List[int]: ...
    def getBytes(self) -> _py_List[int]: ...
    def getColumnCount(self) -> int: ...
    def getDouble(self, int: int, int2: int) -> float: ...
    def getDoubleArray2D(self) -> _py_List[_py_List[float]]: ...
    def getDoubleRow(self, int: int) -> _py_List[float]: ...
    def getDoubles(self) -> _py_List[float]: ...
    def getEnumItem(self, int: int, int2: int) -> 'EnumItem': ...
    def getEnumItemArray2D(self) -> _py_List[_py_List['EnumItem']]: ...
    def getEnumItemRow(self, int: int) -> _py_List['EnumItem']: ...
    def getEnumItemSet(self, int: int, int2: int) -> 'EnumItemSet': ...
    def getEnumItemSetArray2D(self) -> _py_List[_py_List['EnumItemSet']]: ...
    def getEnumItemSetRow(self, int: int) -> _py_List['EnumItemSet']: ...
    def getEnumItemSets(self) -> _py_List['EnumItemSet']: ...
    def getEnumItems(self) -> _py_List['EnumItem']: ...
    def getFloat(self, int: int, int2: int) -> float: ...
    def getFloatArray2D(self) -> _py_List[_py_List[float]]: ...
    def getFloatRow(self, int: int) -> _py_List[float]: ...
    def getFloats(self) -> _py_List[float]: ...
    def getInt(self, int: int, int2: int) -> int: ...
    def getIntArray2D(self) -> _py_List[_py_List[int]]: ...
    def getIntRow(self, int: int) -> _py_List[int]: ...
    def getInternalComponentType(self) -> 'ValueType': ...
    def getInts(self) -> _py_List[int]: ...
    def getLong(self, int: int, int2: int) -> int: ...
    def getLongArray2D(self) -> _py_List[_py_List[int]]: ...
    def getLongRow(self, int: int) -> _py_List[int]: ...
    def getLongs(self) -> _py_List[int]: ...
    def getRowCount(self) -> int: ...
    def getShort(self, int: int, int2: int) -> int: ...
    def getShortArray2D(self) -> _py_List[_py_List[int]]: ...
    def getShortRow(self, int: int) -> _py_List[int]: ...
    def getShorts(self) -> _py_List[int]: ...
    def getString(self, int: int, int2: int) -> str: ...
    def getStringArray2D(self) -> _py_List[_py_List[str]]: ...
    def getStringRow(self, int: int) -> _py_List[str]: ...
    def getStrings(self) -> _py_List[str]: ...

class Array2DBuilder:
    @overload
    def __init__(self, array2D: Array2D): ...
    @overload
    def __init__(self, valueType: 'ValueType', intArray: _py_List[int]): ...
    def build(self) -> Array2D: ...
    def setBoolean(self, boolean: bool, int: int, int2: int) -> 'Array2DBuilder': ...
    def setByte(self, byte: int, int: int, int2: int) -> 'Array2DBuilder': ...
    def setDouble(self, double: float, int: int, int2: int) -> 'Array2DBuilder': ...
    def setEnumItem(self, enumItem: 'EnumItem', int: int, int2: int) -> 'Array2DBuilder': ...
    def setEnumItemSet(self, enumItemSet: 'EnumItemSet', int: int, int2: int) -> 'Array2DBuilder': ...
    def setFloat(self, float: float, int: int, int2: int) -> 'Array2DBuilder': ...
    def setInt(self, int: int, int2: int, int3: int) -> 'Array2DBuilder': ...
    def setLong(self, long: int, int: int, int2: int) -> 'Array2DBuilder': ...
    def setShort(self, short: int, int: int, int2: int) -> 'Array2DBuilder': ...
    def setString(self, string: str, int: int, int2: int) -> 'Array2DBuilder': ...

class BooleanItem:
    def getStandardMeaning(self) -> 'SimpleValueStandardMeaning': ...
    def getValue(self) -> bool: ...

class BooleanType(cern.accsoft.commons.util.Named):
    def getName(self) -> str: ...
    def valueOf(self, boolean: bool) -> BooleanItem: ...
    def values(self) -> java.util.Set[BooleanItem]: ...

class DiscreteFunction(java.lang.Cloneable):
    def clone(self) -> 'DiscreteFunction': ...
    def getX(self, int: int) -> float: ...
    def getY(self, int: int) -> float: ...
    def size(self) -> int: ...
    def toXArray(self) -> _py_List[float]: ...
    def toYArray(self) -> _py_List[float]: ...

class DiscreteFunctionList(java.lang.Cloneable):
    def clone(self) -> 'DiscreteFunctionList': ...
    def getFunction(self, int: int) -> DiscreteFunction: ...
    def iterator(self) -> java.util.Iterator[DiscreteFunction]: ...
    def size(self) -> int: ...

class EnumItem(java.lang.Comparable['EnumItem']):
    def getCode(self) -> int: ...
    def getEnumType(self) -> 'EnumType': ...
    def getStandardMeaning(self) -> 'SimpleValueStandardMeaning': ...
    def getString(self) -> str: ...
    def getSymbol(self) -> str: ...
    def isSettable(self) -> bool: ...

class EnumItemSet(java.util.Set[EnumItem], java.lang.Cloneable):
    @overload
    def addAll(self, enumItemArray: _py_List[EnumItem]) -> bool: ...
    @overload
    def addAll(self, collection: java.util.Collection[_py_Any]) -> bool: ...
    def asLong(self) -> int: ...
    def clone(self) -> 'EnumItemSet': ...
    @overload
    def containsAll(self, enumItemArray: _py_List[EnumItem]) -> bool: ...
    @overload
    def containsAll(self, collection: java.util.Collection[_py_Any]) -> bool: ...
    def equals(self, object: _py_Any) -> bool: ...
    def getEnumType(self) -> 'EnumType': ...
    def hashCode(self) -> int: ...
    @overload
    def removeAll(self, enumItemArray: _py_List[EnumItem]) -> bool: ...
    @overload
    def removeAll(self, collection: java.util.Collection[_py_Any]) -> bool: ...
    @overload
    def retainAll(self, enumItemArray: _py_List[EnumItem]) -> bool: ...
    @overload
    def retainAll(self, collection: java.util.Collection[_py_Any]) -> bool: ...

class EnumType:
    def getBitSize(self) -> 'EnumTypeBitSize': ...
    def getName(self) -> str: ...
    def isApplicableToEnumItemSet(self) -> bool: ...
    def symbols(self) -> java.util.Collection[str]: ...
    @overload
    def valueOf(self, string: str) -> EnumItem: ...
    @overload
    def valueOf(self, long: int) -> EnumItem: ...
    def valueStrings(self) -> java.util.Collection[str]: ...
    def values(self) -> java.util.Set[EnumItem]: ...

class EnumTypeBitSize(java.lang.Enum['EnumTypeBitSize']):
    BYTE: _py_ClassVar['EnumTypeBitSize'] = ...
    SHORT: _py_ClassVar['EnumTypeBitSize'] = ...
    INT: _py_ClassVar['EnumTypeBitSize'] = ...
    LONG: _py_ClassVar['EnumTypeBitSize'] = ...
    STRING: _py_ClassVar['EnumTypeBitSize'] = ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'EnumTypeBitSize': ...
    _valueOf_1__T = _py_TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @classmethod
    @overload
    def valueOf(cls, class_: _py_Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @classmethod
    def values(cls) -> _py_List['EnumTypeBitSize']: ...

class ParameterValue(java.lang.Cloneable):
    def castAsMap(self) -> 'MapParameterValue': ...
    def castAsSimple(self) -> 'SimpleParameterValue': ...
    def clone(self) -> _py_Any: ...
    def getString(self) -> str: ...
    def getType(self) -> 'Type': ...
    def isMutable(self) -> bool: ...
    def makeImmutable(self) -> None: ...
    def makeMutable(self) -> 'ParameterValue': ...

class SimpleValueControlStatus(java.lang.Enum['SimpleValueControlStatus']):
    OK: _py_ClassVar['SimpleValueControlStatus'] = ...
    WARNING: _py_ClassVar['SimpleValueControlStatus'] = ...
    ERROR: _py_ClassVar['SimpleValueControlStatus'] = ...
    NONE: _py_ClassVar['SimpleValueControlStatus'] = ...
    INACTIVE: _py_ClassVar['SimpleValueControlStatus'] = ...
    OFF: _py_ClassVar['SimpleValueControlStatus'] = ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'SimpleValueControlStatus': ...
    _valueOf_1__T = _py_TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @classmethod
    @overload
    def valueOf(cls, class_: _py_Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @classmethod
    def values(cls) -> _py_List['SimpleValueControlStatus']: ...

class SimpleValueStandardMeaning(java.lang.Enum['SimpleValueStandardMeaning']):
    ON: _py_ClassVar['SimpleValueStandardMeaning'] = ...
    OFF: _py_ClassVar['SimpleValueStandardMeaning'] = ...
    WARNING: _py_ClassVar['SimpleValueStandardMeaning'] = ...
    ERROR: _py_ClassVar['SimpleValueStandardMeaning'] = ...
    NONE: _py_ClassVar['SimpleValueStandardMeaning'] = ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'SimpleValueStandardMeaning': ...
    _valueOf_1__T = _py_TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @classmethod
    @overload
    def valueOf(cls, class_: _py_Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @classmethod
    def values(cls) -> _py_List['SimpleValueStandardMeaning']: ...

class SimpleValueStatus(java.io.Serializable, java.lang.Cloneable):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, long: int): ...
    @overload
    def __init__(self, long: int, simpleValueControlStatus: SimpleValueControlStatus): ...
    def equals(self, object: _py_Any) -> bool: ...
    def getAqnStatus(self) -> int: ...
    def getControlStatus(self) -> SimpleValueControlStatus: ...
    def hashCode(self) -> int: ...
    def isBadQuality(self) -> bool: ...
    def isBusy(self) -> bool: ...
    def isDifferentFromSetting(self) -> bool: ...
    def isNotOk(self) -> bool: ...
    def isOutOfRange(self) -> bool: ...
    def isTimeout(self) -> bool: ...
    def toString(self) -> str: ...

class Type(java.io.Serializable):
    SIMPLE: _py_ClassVar['Type'] = ...
    MAP: _py_ClassVar['Type'] = ...
    OBJECT: _py_ClassVar['Type'] = ...
    def toString(self) -> str: ...
    @classmethod
    @overload
    def valueOf(cls, int: int) -> 'Type': ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'Type': ...

class ValueConversionException(java.lang.RuntimeException):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, exception: java.lang.Exception): ...
    @overload
    def __init__(self, string: str): ...
    @overload
    def __init__(self, string: str, exception: java.lang.Exception): ...

class ValueDescriptor:
    def getType(self) -> Type: ...
    def isConstant(self) -> bool: ...

class ValueStatus(java.io.Serializable):
    NONE: _py_ClassVar['ValueStatus'] = ...
    UNDEFINED: _py_ClassVar['ValueStatus'] = ...
    WARNING: _py_ClassVar['ValueStatus'] = ...
    ERROR: _py_ClassVar['ValueStatus'] = ...
    ON: _py_ClassVar['ValueStatus'] = ...
    STAND_BY: _py_ClassVar['ValueStatus'] = ...
    OFF: _py_ClassVar['ValueStatus'] = ...
    DIFFERS_FROM_CONTROL: _py_ClassVar['ValueStatus'] = ...
    OUT_OF_TOLERANCE: _py_ClassVar['ValueStatus'] = ...
    ILLEGAL: _py_ClassVar['ValueStatus'] = ...
    COM_ERROR: _py_ClassVar['ValueStatus'] = ...
    @classmethod
    def convertToValueStatus(cls, int: int) -> 'ValueStatus': ...
    def equals(self, object: _py_Any) -> bool: ...
    def hashCode(self) -> int: ...
    @classmethod
    def mergeValueStatus(cls, valueStatus: 'ValueStatus', valueStatus2: 'ValueStatus') -> 'ValueStatus': ...
    def toString(self) -> str: ...

class ValueType(java.io.Serializable):
    UNDEFINED: _py_ClassVar['ValueType'] = ...
    BOOLEAN: _py_ClassVar['ValueType'] = ...
    BYTE: _py_ClassVar['ValueType'] = ...
    DOUBLE: _py_ClassVar['ValueType'] = ...
    FLOAT: _py_ClassVar['ValueType'] = ...
    INT: _py_ClassVar['ValueType'] = ...
    LONG: _py_ClassVar['ValueType'] = ...
    SHORT: _py_ClassVar['ValueType'] = ...
    STRING: _py_ClassVar['ValueType'] = ...
    BOOLEAN_ARRAY: _py_ClassVar['ValueType'] = ...
    BYTE_ARRAY: _py_ClassVar['ValueType'] = ...
    DOUBLE_ARRAY: _py_ClassVar['ValueType'] = ...
    FLOAT_ARRAY: _py_ClassVar['ValueType'] = ...
    INT_ARRAY: _py_ClassVar['ValueType'] = ...
    LONG_ARRAY: _py_ClassVar['ValueType'] = ...
    SHORT_ARRAY: _py_ClassVar['ValueType'] = ...
    STRING_ARRAY: _py_ClassVar['ValueType'] = ...
    BOOLEAN_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    BYTE_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    DOUBLE_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    FLOAT_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    INT_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    LONG_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    SHORT_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    STRING_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    ENUM: _py_ClassVar['ValueType'] = ...
    ENUM_SET: _py_ClassVar['ValueType'] = ...
    DISCRETE_FUNCTION: _py_ClassVar['ValueType'] = ...
    DISCRETE_FUNCTION_LIST: _py_ClassVar['ValueType'] = ...
    ENUM_ARRAY: _py_ClassVar['ValueType'] = ...
    ENUM_SET_ARRAY: _py_ClassVar['ValueType'] = ...
    ENUM_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    ENUM_SET_ARRAY_2D: _py_ClassVar['ValueType'] = ...
    def equals(self, object: _py_Any) -> bool: ...
    def getComponentType(self) -> 'ValueType': ...
    @classmethod
    def getInternalComponentType(cls, valueType: 'ValueType') -> 'ValueType': ...
    def hashCode(self) -> int: ...
    def isArray(self) -> bool: ...
    def isArray2d(self) -> bool: ...
    @classmethod
    def isArray2dType(cls, valueType: 'ValueType') -> bool: ...
    @classmethod
    def isArrayType(cls, valueType: 'ValueType') -> bool: ...
    def isBoolean(self) -> bool: ...
    @classmethod
    def isBooleanType(cls, valueType: 'ValueType') -> bool: ...
    def isEnumeric(self) -> bool: ...
    @classmethod
    def isEnumericType(cls, valueType: 'ValueType') -> bool: ...
    def isNumeric(self) -> bool: ...
    @classmethod
    def isNumericType(cls, valueType: 'ValueType') -> bool: ...
    def isScalar(self) -> bool: ...
    @classmethod
    def isScalarType(cls, valueType: 'ValueType') -> bool: ...
    def toString(self) -> str: ...
    @classmethod
    @overload
    def valueOf(cls, int: int) -> 'ValueType': ...
    @classmethod
    @overload
    def valueOf(cls, string: str) -> 'ValueType': ...

class ImmutableMapParameterValue(ParameterValue):
    def get(self, string: str) -> 'SimpleParameterValue': ...
    def getArray2D(self, string: str) -> Array2D: ...
    @overload
    def getBoolean(self, string: str) -> bool: ...
    @overload
    def getBoolean(self, string: str, int: int) -> bool: ...
    @overload
    def getBooleans(self, string: str) -> _py_List[bool]: ...
    @overload
    def getBooleans(self, string: str, int: int, int2: int) -> _py_List[bool]: ...
    @overload
    def getByte(self, string: str) -> int: ...
    @overload
    def getByte(self, string: str, int: int) -> int: ...
    @overload
    def getBytes(self, string: str) -> _py_List[int]: ...
    @overload
    def getBytes(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    def getColumnCount(self, string: str) -> int: ...
    def getDiscreteFunction(self, string: str) -> DiscreteFunction: ...
    def getDiscreteFunctionList(self, string: str) -> DiscreteFunctionList: ...
    @overload
    def getDouble(self, string: str) -> float: ...
    @overload
    def getDouble(self, string: str, int: int) -> float: ...
    @overload
    def getDoubles(self, string: str) -> _py_List[float]: ...
    @overload
    def getDoubles(self, string: str, int: int, int2: int) -> _py_List[float]: ...
    def getEnumItem(self, string: str) -> EnumItem: ...
    def getEnumItemSet(self, string: str) -> EnumItemSet: ...
    def getEnumItemSets(self, string: str) -> _py_List[EnumItemSet]: ...
    def getEnumItems(self, string: str) -> _py_List[EnumItem]: ...
    @overload
    def getFloat(self, string: str) -> float: ...
    @overload
    def getFloat(self, string: str, int: int) -> float: ...
    @overload
    def getFloats(self, string: str) -> _py_List[float]: ...
    @overload
    def getFloats(self, string: str, int: int, int2: int) -> _py_List[float]: ...
    @overload
    def getInt(self, string: str) -> int: ...
    @overload
    def getInt(self, string: str, int: int) -> int: ...
    @overload
    def getInts(self, string: str) -> _py_List[int]: ...
    @overload
    def getInts(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    def getLength(self, string: str) -> int: ...
    @overload
    def getLong(self, string: str) -> int: ...
    @overload
    def getLong(self, string: str, int: int) -> int: ...
    @overload
    def getLongs(self, string: str) -> _py_List[int]: ...
    @overload
    def getLongs(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    def getMaxValue(self, string: str) -> float: ...
    def getMinValue(self, string: str) -> float: ...
    def getNames(self) -> _py_List[str]: ...
    @overload
    def getObject(self, string: str) -> _py_Any: ...
    @overload
    def getObject(self, string: str, int: int) -> _py_Any: ...
    def getRowCount(self, string: str) -> int: ...
    @overload
    def getShort(self, string: str) -> int: ...
    @overload
    def getShort(self, string: str, int: int) -> int: ...
    @overload
    def getShorts(self, string: str) -> _py_List[int]: ...
    @overload
    def getShorts(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getString(self, string: str) -> str: ...
    @overload
    def getString(self, string: str, int: int) -> str: ...
    @overload
    def getString(self) -> str: ...
    @overload
    def getStrings(self, string: str) -> _py_List[str]: ...
    @overload
    def getStrings(self, string: str, int: int, int2: int) -> _py_List[str]: ...
    def getUnit(self, string: str) -> str: ...
    def getValueType(self, string: str) -> ValueType: ...
    def getXMaxValue(self, string: str) -> float: ...
    def getXMinValue(self, string: str) -> float: ...
    def getXUnit(self, string: str) -> str: ...
    def getYMaxValue(self, string: str) -> float: ...
    def getYMinValue(self, string: str) -> float: ...
    def getYUnit(self, string: str) -> str: ...
    def size(self) -> int: ...

class MapDescriptor(ValueDescriptor):
    def get(self, string: str) -> 'SimpleDescriptor': ...
    def getNames(self) -> _py_List[str]: ...
    def size(self) -> int: ...

class MapParameterValue(ImmutableMapParameterValue):
    @overload
    def makeMutable(self) -> 'MapParameterValue': ...
    @overload
    def makeMutable(self) -> ParameterValue: ...
    def put(self, string: str, simpleParameterValue: 'SimpleParameterValue') -> None: ...
    def remove(self, string: str) -> 'SimpleParameterValue': ...
    @overload
    def setBoolean(self, string: str, boolean: bool) -> None: ...
    @overload
    def setBoolean(self, string: str, int: int, boolean: bool) -> None: ...
    def setBooleans(self, string: str, booleanArray: _py_List[bool]) -> None: ...
    def setBooleans2D(self, string: str, booleanArray: _py_List[bool], intArray: _py_List[int]) -> None: ...
    @overload
    def setByte(self, string: str, byte: int) -> None: ...
    @overload
    def setByte(self, string: str, int: int, byte: int) -> None: ...
    def setBytes(self, string: str, byteArray: _py_List[int]) -> None: ...
    def setBytes2D(self, string: str, byteArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    def setDiscreteFunction(self, string: str, discreteFunction: DiscreteFunction) -> None: ...
    def setDiscreteFunctionList(self, string: str, discreteFunctionList: DiscreteFunctionList) -> None: ...
    @overload
    def setDouble(self, string: str, double: float) -> None: ...
    @overload
    def setDouble(self, string: str, int: int, double: float) -> None: ...
    def setDoubles(self, string: str, doubleArray: _py_List[float]) -> None: ...
    def setDoubles2D(self, string: str, doubleArray: _py_List[float], intArray: _py_List[int]) -> None: ...
    def setEnumItem(self, string: str, enumItem: EnumItem) -> None: ...
    def setEnumItemSet(self, string: str, enumItemSet: EnumItemSet) -> None: ...
    def setEnumItemSets(self, string: str, enumItemSetArray: _py_List[EnumItemSet]) -> None: ...
    def setEnumItemSets2D(self, string: str, enumItemSetArray: _py_List[EnumItemSet], intArray: _py_List[int]) -> None: ...
    def setEnumItems(self, string: str, enumItemArray: _py_List[EnumItem]) -> None: ...
    def setEnumItems2D(self, string: str, enumItemArray: _py_List[EnumItem], intArray: _py_List[int]) -> None: ...
    @overload
    def setFloat(self, string: str, float: float) -> None: ...
    @overload
    def setFloat(self, string: str, int: int, float: float) -> None: ...
    def setFloats(self, string: str, floatArray: _py_List[float]) -> None: ...
    def setFloats2D(self, string: str, floatArray: _py_List[float], intArray: _py_List[int]) -> None: ...
    @overload
    def setInt(self, string: str, int: int) -> None: ...
    @overload
    def setInt(self, string: str, int: int, int2: int) -> None: ...
    def setInts(self, string: str, intArray: _py_List[int]) -> None: ...
    def setInts2D(self, string: str, intArray: _py_List[int], intArray2: _py_List[int]) -> None: ...
    @overload
    def setLong(self, string: str, int: int, long: int) -> None: ...
    @overload
    def setLong(self, string: str, long: int) -> None: ...
    def setLongs(self, string: str, longArray: _py_List[int]) -> None: ...
    def setLongs2D(self, string: str, longArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    def setObject(self, string: str, object: _py_Any) -> None: ...
    def setObjects2D(self, string: str, object: _py_Any, intArray: _py_List[int]) -> None: ...
    @overload
    def setShort(self, string: str, int: int, short: int) -> None: ...
    @overload
    def setShort(self, string: str, short: int) -> None: ...
    def setShorts(self, string: str, shortArray: _py_List[int]) -> None: ...
    def setShorts2D(self, string: str, shortArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setString(self, string: str, int: int, string2: str) -> None: ...
    @overload
    def setString(self, string: str, string2: str) -> None: ...
    def setStrings(self, string: str, stringArray: _py_List[str]) -> None: ...
    def setStrings2D(self, string: str, stringArray: _py_List[str], intArray: _py_List[int]) -> None: ...

class SimpleDescriptor(MapDescriptor):
    DEFAULT_DECIMAL_FORMAT: _py_ClassVar[str] = ...
    DEFAULT_FORMAT: _py_ClassVar[str] = ...
    DATE_FORMAT_PREFIX: _py_ClassVar[str] = ...
    HEXADECIMAL_FORMAT_PREFIX: _py_ClassVar[str] = ...
    def getBooleanType(self) -> BooleanType: ...
    def getColumnCount(self) -> int: ...
    def getDescription(self) -> str: ...
    def getEnumType(self) -> EnumType: ...
    def getExtraCharacteristic(self, string: str) -> str: ...
    def getExtraCharacteristicNames(self) -> _py_List[str]: ...
    def getFormatPattern(self) -> str: ...
    def getLength(self) -> int: ...
    def getMaxValue(self) -> float: ...
    def getMinValue(self) -> float: ...
    def getRowCount(self) -> int: ...
    def getStandardMeaning(self, object: _py_Any) -> SimpleValueStandardMeaning: ...
    def getTitle(self) -> str: ...
    def getUnit(self) -> str: ...
    def getValueType(self) -> ValueType: ...
    def getXMaxValue(self) -> float: ...
    def getXMinValue(self) -> float: ...
    def getXUnit(self) -> str: ...
    def getYMaxValue(self) -> float: ...
    def getYMinValue(self) -> float: ...
    def getYUnit(self) -> str: ...
    def isDiscrete(self) -> bool: ...
    def isFilterable(self) -> bool: ...
    def isSettable(self, object: _py_Any) -> bool: ...

class SimpleParameterValue(MapParameterValue):
    NO_UNIT: _py_ClassVar[str] = ...
    NO_MIN_MAX_VALUE: _py_ClassVar[float] = ...
    @overload
    def getArray2D(self, string: str) -> Array2D: ...
    @overload
    def getArray2D(self) -> Array2D: ...
    @overload
    def getBoolean(self, string: str) -> bool: ...
    @overload
    def getBoolean(self, string: str, int: int) -> bool: ...
    @overload
    def getBoolean(self) -> bool: ...
    @overload
    def getBoolean(self, int: int) -> bool: ...
    @overload
    def getBooleans(self, string: str) -> _py_List[bool]: ...
    @overload
    def getBooleans(self, string: str, int: int, int2: int) -> _py_List[bool]: ...
    @overload
    def getBooleans(self) -> _py_List[bool]: ...
    @overload
    def getBooleans(self, int: int, int2: int) -> _py_List[bool]: ...
    @overload
    def getByte(self, string: str) -> int: ...
    @overload
    def getByte(self, string: str, int: int) -> int: ...
    @overload
    def getByte(self) -> int: ...
    @overload
    def getByte(self, int: int) -> int: ...
    @overload
    def getBytes(self, string: str) -> _py_List[int]: ...
    @overload
    def getBytes(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getBytes(self) -> _py_List[int]: ...
    @overload
    def getBytes(self, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getColumnCount(self, string: str) -> int: ...
    @overload
    def getColumnCount(self) -> int: ...
    @overload
    def getDiscreteFunction(self, string: str) -> DiscreteFunction: ...
    @overload
    def getDiscreteFunction(self) -> DiscreteFunction: ...
    @overload
    def getDiscreteFunctionList(self, string: str) -> DiscreteFunctionList: ...
    @overload
    def getDiscreteFunctionList(self) -> DiscreteFunctionList: ...
    @overload
    def getDouble(self, string: str) -> float: ...
    @overload
    def getDouble(self, string: str, int: int) -> float: ...
    @overload
    def getDouble(self) -> float: ...
    @overload
    def getDouble(self, int: int) -> float: ...
    @overload
    def getDoubles(self, string: str) -> _py_List[float]: ...
    @overload
    def getDoubles(self, string: str, int: int, int2: int) -> _py_List[float]: ...
    @overload
    def getDoubles(self) -> _py_List[float]: ...
    @overload
    def getDoubles(self, int: int, int2: int) -> _py_List[float]: ...
    @overload
    def getEnumItem(self, string: str) -> EnumItem: ...
    @overload
    def getEnumItem(self) -> EnumItem: ...
    @overload
    def getEnumItem(self, int: int) -> EnumItem: ...
    @overload
    def getEnumItemSet(self, string: str) -> EnumItemSet: ...
    @overload
    def getEnumItemSet(self) -> EnumItemSet: ...
    @overload
    def getEnumItemSet(self, int: int) -> EnumItemSet: ...
    @overload
    def getEnumItemSets(self, string: str) -> _py_List[EnumItemSet]: ...
    @overload
    def getEnumItemSets(self) -> _py_List[EnumItemSet]: ...
    @overload
    def getEnumItemSets(self, int: int, int2: int) -> _py_List[EnumItemSet]: ...
    @overload
    def getEnumItems(self, string: str) -> _py_List[EnumItem]: ...
    @overload
    def getEnumItems(self) -> _py_List[EnumItem]: ...
    @overload
    def getEnumItems(self, int: int, int2: int) -> _py_List[EnumItem]: ...
    @overload
    def getFloat(self, string: str) -> float: ...
    @overload
    def getFloat(self, string: str, int: int) -> float: ...
    @overload
    def getFloat(self) -> float: ...
    @overload
    def getFloat(self, int: int) -> float: ...
    @overload
    def getFloats(self, string: str) -> _py_List[float]: ...
    @overload
    def getFloats(self, string: str, int: int, int2: int) -> _py_List[float]: ...
    @overload
    def getFloats(self) -> _py_List[float]: ...
    @overload
    def getFloats(self, int: int, int2: int) -> _py_List[float]: ...
    @overload
    def getInt(self, string: str) -> int: ...
    @overload
    def getInt(self, string: str, int: int) -> int: ...
    @overload
    def getInt(self) -> int: ...
    @overload
    def getInt(self, int: int) -> int: ...
    @overload
    def getInts(self, string: str) -> _py_List[int]: ...
    @overload
    def getInts(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getInts(self) -> _py_List[int]: ...
    @overload
    def getInts(self, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getLength(self, string: str) -> int: ...
    @overload
    def getLength(self) -> int: ...
    @overload
    def getLong(self, string: str) -> int: ...
    @overload
    def getLong(self, string: str, int: int) -> int: ...
    @overload
    def getLong(self) -> int: ...
    @overload
    def getLong(self, int: int) -> int: ...
    @overload
    def getLongs(self, string: str) -> _py_List[int]: ...
    @overload
    def getLongs(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getLongs(self) -> _py_List[int]: ...
    @overload
    def getLongs(self, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getMaxValue(self, string: str) -> float: ...
    @overload
    def getMaxValue(self) -> float: ...
    @overload
    def getMinValue(self, string: str) -> float: ...
    @overload
    def getMinValue(self) -> float: ...
    @overload
    def getObject(self, string: str) -> _py_Any: ...
    @overload
    def getObject(self, string: str, int: int) -> _py_Any: ...
    @overload
    def getObject(self) -> _py_Any: ...
    @overload
    def getObject(self, int: int) -> _py_Any: ...
    @overload
    def getRowCount(self, string: str) -> int: ...
    @overload
    def getRowCount(self) -> int: ...
    @overload
    def getShort(self, string: str) -> int: ...
    @overload
    def getShort(self, string: str, int: int) -> int: ...
    @overload
    def getShort(self) -> int: ...
    @overload
    def getShort(self, int: int) -> int: ...
    @overload
    def getShorts(self, string: str) -> _py_List[int]: ...
    @overload
    def getShorts(self, string: str, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getShorts(self) -> _py_List[int]: ...
    @overload
    def getShorts(self, int: int, int2: int) -> _py_List[int]: ...
    @overload
    def getString(self, string: str) -> str: ...
    @overload
    def getString(self, string: str, int: int) -> str: ...
    @overload
    def getString(self) -> str: ...
    @overload
    def getString(self, int: int) -> str: ...
    @overload
    def getStrings(self, string: str) -> _py_List[str]: ...
    @overload
    def getStrings(self, string: str, int: int, int2: int) -> _py_List[str]: ...
    @overload
    def getStrings(self) -> _py_List[str]: ...
    @overload
    def getStrings(self, int: int, int2: int) -> _py_List[str]: ...
    @overload
    def getUnit(self, string: str) -> str: ...
    @overload
    def getUnit(self) -> str: ...
    def getValueStatus(self) -> SimpleValueStatus: ...
    @overload
    def getValueType(self, string: str) -> ValueType: ...
    @overload
    def getValueType(self) -> ValueType: ...
    @overload
    def getXMaxValue(self, string: str) -> float: ...
    @overload
    def getXMaxValue(self) -> float: ...
    @overload
    def getXMinValue(self, string: str) -> float: ...
    @overload
    def getXMinValue(self) -> float: ...
    @overload
    def getXUnit(self, string: str) -> str: ...
    @overload
    def getXUnit(self) -> str: ...
    @overload
    def getYMaxValue(self, string: str) -> float: ...
    @overload
    def getYMaxValue(self) -> float: ...
    @overload
    def getYMinValue(self, string: str) -> float: ...
    @overload
    def getYMinValue(self) -> float: ...
    @overload
    def getYUnit(self, string: str) -> str: ...
    @overload
    def getYUnit(self) -> str: ...
    @overload
    def makeMutable(self) -> 'SimpleParameterValue': ...
    @overload
    def makeMutable(self) -> MapParameterValue: ...
    @overload
    def makeMutable(self) -> ParameterValue: ...
    @overload
    def setBoolean(self, string: str, boolean: bool) -> None: ...
    @overload
    def setBoolean(self, string: str, int: int, boolean: bool) -> None: ...
    @overload
    def setBoolean(self, boolean: bool) -> None: ...
    @overload
    def setBoolean(self, int: int, boolean: bool) -> None: ...
    @overload
    def setBooleans(self, string: str, booleanArray: _py_List[bool]) -> None: ...
    @overload
    def setBooleans(self, booleanArray: _py_List[bool]) -> None: ...
    @overload
    def setBooleans2D(self, string: str, booleanArray: _py_List[bool], intArray: _py_List[int]) -> None: ...
    @overload
    def setBooleans2D(self, booleanArray: _py_List[bool], intArray: _py_List[int]) -> None: ...
    @overload
    def setByte(self, string: str, byte: int) -> None: ...
    @overload
    def setByte(self, string: str, int: int, byte: int) -> None: ...
    @overload
    def setByte(self, byte: int) -> None: ...
    @overload
    def setByte(self, int: int, byte: int) -> None: ...
    @overload
    def setBytes(self, string: str, byteArray: _py_List[int]) -> None: ...
    @overload
    def setBytes(self, byteArray: _py_List[int]) -> None: ...
    @overload
    def setBytes2D(self, string: str, byteArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setBytes2D(self, byteArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setDiscreteFunction(self, string: str, discreteFunction: DiscreteFunction) -> None: ...
    @overload
    def setDiscreteFunction(self, discreteFunction: DiscreteFunction) -> None: ...
    @overload
    def setDiscreteFunctionList(self, string: str, discreteFunctionList: DiscreteFunctionList) -> None: ...
    @overload
    def setDiscreteFunctionList(self, discreteFunctionList: DiscreteFunctionList) -> None: ...
    @overload
    def setDouble(self, string: str, double: float) -> None: ...
    @overload
    def setDouble(self, string: str, int: int, double: float) -> None: ...
    @overload
    def setDouble(self, double: float) -> None: ...
    @overload
    def setDouble(self, int: int, double: float) -> None: ...
    @overload
    def setDoubles(self, string: str, doubleArray: _py_List[float]) -> None: ...
    @overload
    def setDoubles(self, doubleArray: _py_List[float]) -> None: ...
    @overload
    def setDoubles2D(self, string: str, doubleArray: _py_List[float], intArray: _py_List[int]) -> None: ...
    @overload
    def setDoubles2D(self, doubleArray: _py_List[float], intArray: _py_List[int]) -> None: ...
    @overload
    def setEnumItem(self, string: str, enumItem: EnumItem) -> None: ...
    @overload
    def setEnumItem(self, enumItem: EnumItem) -> None: ...
    @overload
    def setEnumItem(self, int: int, enumItem: EnumItem) -> None: ...
    @overload
    def setEnumItemSet(self, string: str, enumItemSet: EnumItemSet) -> None: ...
    @overload
    def setEnumItemSet(self, enumItemSet: EnumItemSet) -> None: ...
    @overload
    def setEnumItemSet(self, int: int, enumItemSet: EnumItemSet) -> None: ...
    @overload
    def setEnumItemSets(self, string: str, enumItemSetArray: _py_List[EnumItemSet]) -> None: ...
    @overload
    def setEnumItemSets(self, enumItemSetArray: _py_List[EnumItemSet]) -> None: ...
    @overload
    def setEnumItemSets2D(self, string: str, enumItemSetArray: _py_List[EnumItemSet], intArray: _py_List[int]) -> None: ...
    @overload
    def setEnumItemSets2D(self, enumItemSetArray: _py_List[EnumItemSet], intArray: _py_List[int]) -> None: ...
    @overload
    def setEnumItems(self, string: str, enumItemArray: _py_List[EnumItem]) -> None: ...
    @overload
    def setEnumItems(self, enumItemArray: _py_List[EnumItem]) -> None: ...
    @overload
    def setEnumItems2D(self, string: str, enumItemArray: _py_List[EnumItem], intArray: _py_List[int]) -> None: ...
    @overload
    def setEnumItems2D(self, enumItemArray: _py_List[EnumItem], intArray: _py_List[int]) -> None: ...
    @overload
    def setFloat(self, string: str, float: float) -> None: ...
    @overload
    def setFloat(self, string: str, int: int, float: float) -> None: ...
    @overload
    def setFloat(self, float: float) -> None: ...
    @overload
    def setFloat(self, int: int, float: float) -> None: ...
    @overload
    def setFloats(self, string: str, floatArray: _py_List[float]) -> None: ...
    @overload
    def setFloats(self, floatArray: _py_List[float]) -> None: ...
    @overload
    def setFloats2D(self, string: str, floatArray: _py_List[float], intArray: _py_List[int]) -> None: ...
    @overload
    def setFloats2D(self, floatArray: _py_List[float], intArray: _py_List[int]) -> None: ...
    @overload
    def setInt(self, string: str, int: int) -> None: ...
    @overload
    def setInt(self, string: str, int: int, int2: int) -> None: ...
    @overload
    def setInt(self, int: int) -> None: ...
    @overload
    def setInt(self, int: int, int2: int) -> None: ...
    @overload
    def setInts(self, string: str, intArray: _py_List[int]) -> None: ...
    @overload
    def setInts(self, intArray: _py_List[int]) -> None: ...
    @overload
    def setInts2D(self, string: str, intArray: _py_List[int], intArray2: _py_List[int]) -> None: ...
    @overload
    def setInts2D(self, intArray: _py_List[int], intArray2: _py_List[int]) -> None: ...
    @overload
    def setLong(self, string: str, int: int, long: int) -> None: ...
    @overload
    def setLong(self, string: str, long: int) -> None: ...
    @overload
    def setLong(self, int: int, long: int) -> None: ...
    @overload
    def setLong(self, long: int) -> None: ...
    @overload
    def setLongs(self, string: str, longArray: _py_List[int]) -> None: ...
    @overload
    def setLongs(self, longArray: _py_List[int]) -> None: ...
    @overload
    def setLongs2D(self, string: str, longArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setLongs2D(self, longArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setObject(self, string: str, object: _py_Any) -> None: ...
    @overload
    def setObject(self, object: _py_Any) -> None: ...
    @overload
    def setObjects2D(self, string: str, object: _py_Any, intArray: _py_List[int]) -> None: ...
    @overload
    def setObjects2D(self, object: _py_Any, intArray: _py_List[int]) -> None: ...
    @overload
    def setShort(self, string: str, int: int, short: int) -> None: ...
    @overload
    def setShort(self, string: str, short: int) -> None: ...
    @overload
    def setShort(self, int: int, short: int) -> None: ...
    @overload
    def setShort(self, short: int) -> None: ...
    @overload
    def setShorts(self, string: str, shortArray: _py_List[int]) -> None: ...
    @overload
    def setShorts(self, shortArray: _py_List[int]) -> None: ...
    @overload
    def setShorts2D(self, string: str, shortArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setShorts2D(self, shortArray: _py_List[int], intArray: _py_List[int]) -> None: ...
    @overload
    def setString(self, string: str, int: int, string2: str) -> None: ...
    @overload
    def setString(self, string: str, string2: str) -> None: ...
    @overload
    def setString(self, int: int, string: str) -> None: ...
    @overload
    def setString(self, string: str) -> None: ...
    @overload
    def setStrings(self, string: str, stringArray: _py_List[str]) -> None: ...
    @overload
    def setStrings(self, stringArray: _py_List[str]) -> None: ...
    @overload
    def setStrings2D(self, string: str, stringArray: _py_List[str], intArray: _py_List[int]) -> None: ...
    @overload
    def setStrings2D(self, stringArray: _py_List[str], intArray: _py_List[int]) -> None: ...
    def setValueStatus(self, simpleValueStatus: SimpleValueStatus) -> None: ...
