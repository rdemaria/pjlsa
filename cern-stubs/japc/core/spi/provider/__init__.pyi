from typing import List as _py_List
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.japc.core
import cern.japc.core.spi.cache
import cern.japc.value
import java.util


class CompositeParameterExplorer(cern.japc.core.ParameterExplorer):
    def __init__(self, parameterExplorerArray: _py_List[cern.japc.core.ParameterExplorer]): ...
    def getAcceleratorNames(self, boolean: bool) -> _py_List[str]: ...
    def getDependentParameterNames(self, string: str) -> _py_List[str]: ...
    def getDeviceNamesForAccelerator(self, string: str, filter: cern.japc.core.ParameterExplorer.Filter) -> _py_List[str]: ...
    def getDeviceNamesForHost(self, string: str, filter: cern.japc.core.ParameterExplorer.Filter) -> _py_List[str]: ...
    def getFamilyNamesForAccelerator(self, string: str, filter: cern.japc.core.ParameterExplorer.Filter) -> _py_List[str]: ...
    def getFamilyNamesForHost(self, string: str, filter: cern.japc.core.ParameterExplorer.Filter) -> _py_List[str]: ...
    def getHostNamesForAccelerator(self, string: str, filter: cern.japc.core.ParameterExplorer.Filter) -> _py_List[str]: ...
    def getPropertyNameDescriptions(self, string: str) -> _py_List[cern.japc.core.ParameterExplorer.NameDescription]: ...
    def getRootParameterNames(self) -> _py_List[str]: ...
    def getSourceParameterNames(self, string: str) -> _py_List[str]: ...
    @classmethod
    @overload
    def mergeResults(cls, nameDescriptionArray: _py_List[_py_List[cern.japc.core.ParameterExplorer.NameDescription]], int: int) -> _py_List[cern.japc.core.ParameterExplorer.NameDescription]: ...
    @classmethod
    @overload
    def mergeResults(cls, stringArray: _py_List[_py_List[str]], int: int) -> _py_List[str]: ...

class DescriptorContainer:
    def getDeviceDescriptors(self) -> java.util.Map[str, cern.japc.core.DeviceDescriptor]: ...
    def getParameterDescriptors(self) -> java.util.Map[str, cern.japc.core.ParameterDescriptor]: ...
    def getValueDescriptors(self) -> java.util.Map[str, cern.japc.value.ValueDescriptor]: ...

class DescriptorProvider:
    SYSPROP_CDP_DATA_CONTAINERS: _py_ClassVar[str] = ...
    def findDeviceDescriptor(self, string: str) -> cern.japc.core.DeviceDescriptor: ...
    def findDeviceDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.DeviceDescriptor]: ...
    def findParameterDescriptor(self, string: str) -> cern.japc.core.ParameterDescriptor: ...
    def findParameterDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.ParameterDescriptor]: ...
    def findValueDescriptor(self, string: str) -> cern.japc.value.ValueDescriptor: ...
    def findValueDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.value.ValueDescriptor]: ...

class CachingDescriptorProvider(DescriptorProvider, cern.japc.core.spi.cache.JapcCache):
    def __init__(self, descriptorProvider: DescriptorProvider): ...
    def clearAll(self) -> None: ...
    def findDeviceDescriptor(self, string: str) -> cern.japc.core.DeviceDescriptor: ...
    def findDeviceDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.DeviceDescriptor]: ...
    def findParameterDescriptor(self, string: str) -> cern.japc.core.ParameterDescriptor: ...
    def findParameterDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.ParameterDescriptor]: ...
    def findValueDescriptor(self, string: str) -> cern.japc.value.ValueDescriptor: ...
    def findValueDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.value.ValueDescriptor]: ...

class CompositeDescriptorProvider(DescriptorProvider):
    def __init__(self, descriptorProviderArray: _py_List[DescriptorProvider]): ...
    def createParameterExplorer(self) -> cern.japc.core.ParameterExplorer: ...
    def findDeviceDescriptor(self, string: str) -> cern.japc.core.DeviceDescriptor: ...
    def findDeviceDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.DeviceDescriptor]: ...
    def findParameterDescriptor(self, string: str) -> cern.japc.core.ParameterDescriptor: ...
    def findParameterDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.ParameterDescriptor]: ...
    def findValueDescriptor(self, string: str) -> cern.japc.value.ValueDescriptor: ...
    def findValueDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.value.ValueDescriptor]: ...

class DefaultDescriptorProvider(DescriptorProvider):
    def __init__(self): ...
    @classmethod
    def createDefaultParameterDescriptor(cls, string: str) -> cern.japc.core.ParameterDescriptor: ...
    def createParameterExplorer(self) -> cern.japc.core.ParameterExplorer: ...
    def findDeviceDescriptor(self, string: str) -> cern.japc.core.DeviceDescriptor: ...
    def findDeviceDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.DeviceDescriptor]: ...
    def findParameterDescriptor(self, string: str) -> cern.japc.core.ParameterDescriptor: ...
    def findParameterDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.core.ParameterDescriptor]: ...
    def findValueDescriptor(self, string: str) -> cern.japc.value.ValueDescriptor: ...
    def findValueDescriptors(self, collection: java.util.Collection[str]) -> java.util.Map[str, cern.japc.value.ValueDescriptor]: ...

class CustomizableDescriptorProvider(DefaultDescriptorProvider):
    DATA_CONTAINER_SEPARATOR: _py_ClassVar[str] = ...
    def __init__(self): ...
    @classmethod
    def addAllDescriptors(cls, string: str, deviceDescriptor: cern.japc.core.DeviceDescriptor, parameterDescriptor: cern.japc.core.ParameterDescriptor, valueDescriptor: cern.japc.value.ValueDescriptor) -> None: ...
    @classmethod
    def addDeviceDescriptor(cls, string: str, deviceDescriptor: cern.japc.core.DeviceDescriptor) -> None: ...
    @classmethod
    def addParameterAndValueDescriptor(cls, string: str, parameterDescriptor: cern.japc.core.ParameterDescriptor, valueDescriptor: cern.japc.value.ValueDescriptor) -> None: ...
    @classmethod
    def addParameterDescriptor(cls, string: str, parameterDescriptor: cern.japc.core.ParameterDescriptor) -> None: ...
    @classmethod
    def addValueDescriptor(cls, string: str, valueDescriptor: cern.japc.value.ValueDescriptor) -> None: ...
    def findDeviceDescriptor(self, string: str) -> cern.japc.core.DeviceDescriptor: ...
    def findParameterDescriptor(self, string: str) -> cern.japc.core.ParameterDescriptor: ...
    def findValueDescriptor(self, string: str) -> cern.japc.value.ValueDescriptor: ...
    @classmethod
    def getDeviceInstanceSeparator(cls) -> str: ...
    @classmethod
    def setDeviceInstanceSeparator(cls, string: str) -> None: ...
