from typing import List as _py_List
from typing import ClassVar as _py_ClassVar
from typing import overload
import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.zones
import cern.accsoft.commons.util
import java.util


class ParticleTransfer(cern.accsoft.commons.util.Named):
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> 'ParticleTransferType': ...

class ParticleTransferType(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['ParticleTransferType']):
    RING: _py_ClassVar['ParticleTransferType'] = ...
    TRANSFER: _py_ClassVar['ParticleTransferType'] = ...
    SOURCE: _py_ClassVar['ParticleTransferType'] = ...
    EXPERIMENT: _py_ClassVar['ParticleTransferType'] = ...
    COOLER: _py_ClassVar['ParticleTransferType'] = ...
    @classmethod
    def valueOf(cls, string: str) -> 'ParticleTransferType': ...
    @classmethod
    def values(cls) -> _py_List['ParticleTransferType']: ...

class AdParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['AdParticleTransfer'], ParticleTransfer):
    ADRING: _py_ClassVar['AdParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'AdParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['AdParticleTransfer']: ...

class AwakeParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['AwakeParticleTransfer'], ParticleTransfer):
    AWAKE: _py_ClassVar['AwakeParticleTransfer'] = ...
    AWAKEElectronTransfer: _py_ClassVar['AwakeParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'AwakeParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['AwakeParticleTransfer']: ...

class CtfParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['CtfParticleTransfer'], ParticleTransfer):
    CTF: _py_ClassVar['CtfParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'CtfParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['CtfParticleTransfer']: ...

class ElenaParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['ElenaParticleTransfer'], ParticleTransfer):
    ELENA: _py_ClassVar['ElenaParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'ElenaParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['ElenaParticleTransfer']: ...

class IsoldeParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['IsoldeParticleTransfer'], ParticleTransfer):
    ISOLDE: _py_ClassVar['IsoldeParticleTransfer'] = ...
    HIE: _py_ClassVar['IsoldeParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'IsoldeParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['IsoldeParticleTransfer']: ...

class LeirParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['LeirParticleTransfer'], ParticleTransfer):
    LEIRRING: _py_ClassVar['LeirParticleTransfer'] = ...
    LINAC3: _py_ClassVar['LeirParticleTransfer'] = ...
    LEIRInjection: _py_ClassVar['LeirParticleTransfer'] = ...
    LEIREjection: _py_ClassVar['LeirParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'LeirParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['LeirParticleTransfer']: ...

class LhcParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['LhcParticleTransfer'], ParticleTransfer):
    LHCRING: _py_ClassVar['LhcParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'LhcParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['LhcParticleTransfer']: ...

class NorthParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['NorthParticleTransfer'], ParticleTransfer):
    H2: _py_ClassVar['NorthParticleTransfer'] = ...
    H4: _py_ClassVar['NorthParticleTransfer'] = ...
    H6: _py_ClassVar['NorthParticleTransfer'] = ...
    H8: _py_ClassVar['NorthParticleTransfer'] = ...
    K12: _py_ClassVar['NorthParticleTransfer'] = ...
    M2: _py_ClassVar['NorthParticleTransfer'] = ...
    T2: _py_ClassVar['NorthParticleTransfer'] = ...
    T4: _py_ClassVar['NorthParticleTransfer'] = ...
    T6: _py_ClassVar['NorthParticleTransfer'] = ...
    T10: _py_ClassVar['NorthParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'NorthParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['NorthParticleTransfer']: ...

class PsParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['PsParticleTransfer'], ParticleTransfer):
    PSExtractionTT2: _py_ClassVar['PsParticleTransfer'] = ...
    PSRING: _py_ClassVar['PsParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'PsParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['PsParticleTransfer']: ...

class PsbParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['PsbParticleTransfer'], ParticleTransfer):
    LINAC4: _py_ClassVar['PsbParticleTransfer'] = ...
    PSBInjection: _py_ClassVar['PsbParticleTransfer'] = ...
    PSBRING: _py_ClassVar['PsbParticleTransfer'] = ...
    PSBExtraction: _py_ClassVar['PsbParticleTransfer'] = ...
    PSTransfer: _py_ClassVar['PsbParticleTransfer'] = ...
    GPSTransfer: _py_ClassVar['PsbParticleTransfer'] = ...
    HRSTransfer: _py_ClassVar['PsbParticleTransfer'] = ...
    PSBDumpTransfer: _py_ClassVar['PsbParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'PsbParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['PsbParticleTransfer']: ...

class SpsParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['SpsParticleTransfer'], ParticleTransfer):
    AWAKETransfer: _py_ClassVar['SpsParticleTransfer'] = ...
    EastExtraction: _py_ClassVar['SpsParticleTransfer'] = ...
    HiRadMatTransfer: _py_ClassVar['SpsParticleTransfer'] = ...
    LHCB1Transfer: _py_ClassVar['SpsParticleTransfer'] = ...
    LHCB2Transfer: _py_ClassVar['SpsParticleTransfer'] = ...
    NorthExtraction: _py_ClassVar['SpsParticleTransfer'] = ...
    SPSInjection: _py_ClassVar['SpsParticleTransfer'] = ...
    SPSRING: _py_ClassVar['SpsParticleTransfer'] = ...
    T2Transfer: _py_ClassVar['SpsParticleTransfer'] = ...
    T4Transfer: _py_ClassVar['SpsParticleTransfer'] = ...
    T6Transfer: _py_ClassVar['SpsParticleTransfer'] = ...
    WestExtraction: _py_ClassVar['SpsParticleTransfer'] = ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    @overload
    def getAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator: ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDescription(self) -> str: ...
    def getParticleTransferType(self) -> ParticleTransferType: ...
    @classmethod
    def valueOf(cls, string: str) -> 'SpsParticleTransfer': ...
    @classmethod
    def values(cls) -> _py_List['SpsParticleTransfer']: ...
