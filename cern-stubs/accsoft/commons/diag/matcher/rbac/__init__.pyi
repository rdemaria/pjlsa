from typing import ClassVar as _py_ClassVar
import cern.accsoft.commons.diag.matcher


class AbstractRbacStringThrowableMatcher(cern.accsoft.commons.diag.matcher.StringThrowableMatcher):
    RBAC_PROBLEM_DOMAIN: _py_ClassVar[str] = ...

class RbacCmw3SecurityExceptionStringThrowableMatcher(AbstractRbacStringThrowableMatcher):
    RBAC_CMW_SECURITY_MATCHER_NAME: _py_ClassVar[str] = ...
    def __init__(self): ...

class RbacStringThrowableMatcher(AbstractRbacStringThrowableMatcher):
    RBAC_MATCHER_NAME: _py_ClassVar[str] = ...
    def __init__(self): ...
